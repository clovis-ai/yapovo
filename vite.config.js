/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */
const path = require('path')
const srcPath = path.resolve(__dirname, './')
require('dotenv').config()

module.exports = {
	server: {
		open: false, // do not open the browser as we use electron
		port: process.env.VITE_PORT,
	},
	alias: {
		// setup aliases for cleaner imports
		'/~': srcPath,
	},
	plugins: [],
	optimizeDeps: {
		// exclude path and electron-window-state as we are using the node runtime inside the browser
		// and don't wont vite to complain. If you have any issues importing node packages and vite complains,
		// add them here
		exclude: ['path', 'electron-window-state'],
	},
}