/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import html from '@web/rollup-plugin-html'
import babel from "@rollup/plugin-babel"
import json from '@rollup/plugin-json'

export default [

	{
		input: 'control/yapovo-electron/control_panel.html',
		output: [
			{dir: 'control/yapovo-electron/dist', format: 'es'},
		],
		plugins: [html(), babel(), resolve(), commonjs()],
	},
	{
		input: 'common/yapovo-drawing/index.html',
		output: [
			{dir: 'control/yapovo-electron/dist', format: 'es'},
		],
		plugins: [html(), babel(), resolve(), commonjs()],
	},
	{
		input: 'output/yapovo-output/output_panel.html',
		output: [
			{dir: 'control/yapovo-electron/dist', format: 'es'},
		],
		plugins: [html(), babel(), resolve(), commonjs()],
	},

	{
		input: 'control/yapovo-nodejs/node_process.js',
		output: [
			{dir: 'control/yapovo-electron/dist', format: 'cjs'},
		],
		plugins: [json(), babel(), commonjs(), resolve()],
	},
]
