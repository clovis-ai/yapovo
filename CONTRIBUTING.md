# Contribuer

## Paramétrage du projet

Ce projet utilise des _forks_, qui correspondent à des copies du projet.

Chaque membre travaille sur son propre dépôt, ce qui lui permet de supprimer ses branches, etc, sans craindre de gêner les autres membres.
Par convention, nous décidons de ne pas dépendre des branches d'autres personnes : on garde en tête la possibilité que n'importe qui puisse supprimer une de ses branches à n'importe quel moment.

Chaque personne va donc devoir paramétrer son propre projet :
- Pour cela, [créer un nouveau fork](https://gitlab.com/clovis-ai/yapovo/-/forks/new) et choisir son propre compte
- Aller dans la page 'members' (colonne à gauche)
- Ajouter les autres développeurs du projet en tant que 'reporter' (inutile d'ajouter les encadrants). Cela leur permet de lire votre code, mais pas de le modifier.
- Dans 'Settings → General → Project Features', désactiver 'Wiki' et 'Snippets', puis 'save changes'
- Dans 'Settings → General → Merge Requests', vérifier que :
  - 'Merge method' : 'merge commit',
  - 'Merge options' : cocher 'show link to create…', cocher 'Enable Delete source branch by default'
  - 'Squash commits' : 'Do not allow'
  - 'Merge checks' : cocher 'pipelines must succeed' (pas avant que les pipelines soient paramétrées), cocher 'all discussions must be resolved'
- Dans 'Settings → Repository → Protected Branches', pour la branche principale :
  - 'Allowed to merge' : 'Maintainers'
  - 'Allowed to push' : 'No one'

Le projet est maintenant paramétré, vous pouvez le récupérer en local :
- Si vous n'avez pas paramétré les clefs SSH, [faites-le](https://gitlab.com/-/profile/keys)
- `git clone git@gitlab.com:clovis-ai/yapovo.git <là où vous voulez travailler>`
- `cd <dossier du projet>`
- `git remote rename origin general` pour renommer la remote par défaut
- `git remote add <nom> <url>` pour se connecter aux dépôts des autres membres (par exemple, `git remote add ivan git@gitlab.com:clovis-ai/yapovo-clovis.git`)
- Quand tous les dépôts sont ajoutés, vous pouvez exécuter `git fetch --all`
- Maintenant, vous pouvez utiliser :
  - `git branch -vv` pour voir vos branches
  - `git branch -vva` pour voir les branches de tout le monde

À noter qu'il est fortement déconseillé de `switch` sur une branche qui ne vous appartient pas : son propriétaire peut la supprimer à tout moment. Cet accès aux branches est juste là à titre indicatif pour voir ce que les gens font, ne faites pas dépendre votre code dessus.

## Contribuer

Commencez par choisir sur quelle [tâche](https://gitlab.com/clovis-ai/yapovo/-/issues) vous allez travailler.
- `git fetch general`
- `git switch -b XX-branche general/master` (pour les anciennes versions : `git checkout -b XX-branche general/master`) crée une branche `XX-branche` à partir du commit le plus récent de `master`, et vous déplace dedans. Le nom de votre branche doit commencer par le numéro de la tâche, suivi par quelques mots pour la reconnaître facilement (par exemple pour la tâche `#2 Paramétrer Git`, vous pouvez appeler votre branche `2-git` ou `2-setup-git`). Cela va permettre à GitLab de lier automatiquement la tâche à la branche, pour que la tâche soit validée automatiquement quand on va valider la branche.

La création de commit doit suivre les conventions suivantes (inspirées de 'Conventional commits', des conventions 'Angular' et des conventions 'ESLint').
Un commit doit commencer par une ligne de header, du style `type(scope): Description` ou `type: Description`.
Les types acceptés sont :
- `build` ou `Build` pour les modifications des outils de build (Makefile, …)
- `upgrade` ou `Upgrade` pour des modifications des dépendances du projet
- `ci` pour des modifications des pipelines CI/CD
- `doc`, `docs` ou `Docs` pour des modifications de la documentation (`README.md`, …)
- `feat` ou `New` pour des ajouts
- `fix`, `Fix` ou `fixes` pour la résolution de problèmes ou bugs
- `perf`, `perfs` ou `Update` pour des améliorations des performances qui n'ajoutent rien de nouveau
- `refactor` ou `Update` pour des modifications internes qui n'ajoutent rien de nouveau
- `style` pour des modifications du style, les fautes d'orthographe…
- `test` ou `tests` pour des modifications des tests
- `merge` pour la fusion d'une branche (autorisé uniquement sur `master`)

Le 'scope' d'un commit doit être composé d'un seul mot représentant la partie du projet impactée. Par exemple, sur un projet ayant un client et un serveur, `client` et `server` sont des scopes valides. Les scopes ne doivent jamais être un nom de fichier ou autre, l'idée est de donner une idée générale de la zone modifiée.

Si vous n'arrivez pas à choisir un seul 'scope' ou un seul 'type' à votre commit, c'est qu'il est trop gros. Séparez votre commit en plusieurs plus petits.

Le commit est ensuite suivi d'un petit message de description, et si nécessaire, d'un paragraphe expliquant les modifications plus en détails.
- Si la description mentionne une modification du code, elle doit être écrite au passé : `Implemented screen sharing`,
- Si la description mentionne une nouvelle feature, elle doit être écrite au présent : `The button 'save' now works`
- De manière générale, la description doit expliquer l'utilité du commit, pas comment cela est fait. Par exemple, `Now forbidden to create a user without a name` est correct, `Added if` est incorrect.

Petit exemple valide :
```text
fix(client): No crash anymore when saving a user
```

Exemple avec plus de détails, ici pour merge une MR (avec le type spécifique `merge`) :
```text
merge(server): Added parallelism in connection

The previous implementation could deadlock. By replacing locks with semaphores, it's now impossible.

Closes #5
```

Pour partager votre code :
- Faire un `git push --set-upstream <votre nom> <votre branche>`
- Dans la console, un lien apparait, cliquez dessus, puis remplissez les informations suivantes :
- "From `<votre nom>/<votre fork>:<votre branche>` Into `clovis-ai/yapovo:master`"
- **Title** : si votre code n'est pas complètement terminé, commencez le titre par "Draft: ", sinon expliquez en quelques mots ce que votre MR change
- **Description** : Si votre MR ajoute des parties importantes au projet qui ne sont pas claires juste en lisant le code (par exemple, de nouveaux outils, de nouvelles commandes…), expliquez-le ici. Si votre MR valide une issue, vérifiez que la description se termine par `Closes #<numéro>`
- **Assignee** : La personne responsable du code, donc vous
- **Reviewer** : La personne responsable de vérifier le code
- Si tout s'est bien passé, votre MR est maintenant affichée [ici](https://gitlab.com/clovis-ai/yapovo/-/merge_requests).

Vous pouvez continuer à travailler dans cette branche, et vous pouvez simplement exécuter `git push` pour la mettre à jour.
Quand votre code est prêt, n'oubliez pas d'enlever le 'Draft: ' du titre de votre MR.

## Review

Pendant la phase de review, un ou plusieurs membres vont écrire des commentaires sur votre code.

À garder en tête :
- Les commits ne doivent pas contenir de `merge`.
- Tout commit `fix` ou similaire doit régler un problème datant d'_avant_ la branche (eg. un problème sur `master`).
- Si GitLab vous informe que votre branche comporte des conflits :
  - Dans un IDE JetBrains : `SHIFT SHIFT`, taper `fetch`; puis `SHIFT SHIFT`, taper `rebase`, sélectionner la branche `general/master`, dans 'modify options' ajouter `--interactive`, puis suivre les étapes données par l'IDE. [cliquez ici si vous avez besoin d'aide](https://www.jetbrains.com/help/idea/edit-project-history.html)
  - Dans le terminal : `git pull --rebase=interactive general/master` [cliquez ici si vous avez besoin d'aide](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)

Si les reviewers vous demandent de modifier quelque chose :
- Créez un commit contenant cette modification (_un commit par modification_)
- Combinez les solutions avec les commits qui ont créé le problème :
  - Dans un IDE JetBrains : `SHIFT SHIFT`, taper `show git log`, clic-droit sur le commit le plus ancien de cette branche (le premier), puis `Interactively rebase from here`. [Tout est expliqué ici](https://www.jetbrains.com/help/idea/edit-project-history.html#interactive-rebase)
  - Dans le terminal : `git rebase -i HEAD~n` où `n` est le nombre de commits dans la branche (par exemple `git rebase -i HEAD~5`). [Tout est expliqué ici](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)

Attention, un `rebase` nécessite de faire un `push --force`. L'utilisation de forks permet d'empêcher de casser le code de quelqu'un d'autre (vous n'avez pas les droits dessus), mais vous pouvez quand même perdre votre propre code. Si vous avez le moindre doute, lisez la documentation. Si vous perdez quelque chose, c'est votre propre responsabilité.

Les guidelines pour la review :
- Le 'reviewer' est la seule personne à avoir le droit d'appuyer sur le bouton 'merge'. Attention, le commit généré par défaut par GitLab ne respecte pas les conventions : il faut donc appuyer sur le bouton 'modify merge commit' puis 'include merge request description' (sinon le merge sera rejeté)
- Lors d'une demande de modification, la personne ayant effectué la demande est la seule qui a le droit d'appuyer sur le bouton 'resolve'. Cela permet de vérifier que la solution lui correspond.

## À noter

La commande `git pull` crée par défaut un commit de `merge`, qui rend les `rebase` plus compliqués.
Un commit de merge permet de fusionner deux branches sans en modifier aucune. Dans un projet bien organisé, cette commande est inutile dans une branche autre que `master` : on modifie les branches non-terminées jusqu'à ce qu'elles soient parfaites, puis on les `merge` dans `master`, où elles ne seront plus jamais modifiées.
Puisque c'est GitLab qui va s'occuper des fusions dans notre cas, il ne sera jamais nécessaire de faire un merge (sauf cas très particuliers, mais c'est vraiment rare).

Il est possible de changer le comportement par défaut pour que `git pull` exécute un `rebase` (enlever le `global` pour que cela s'applique uniquement au dépôt courant) :
```bash
git config --global pull.rebase true
```

N'hésitez pas à créer des alias pour les commandes que vous utilisez souvent :
```bash
git config --global alias.<alias> '<commande>'

# Par exemple
git config --global alias.st 'status -sb --untracked=all'
```

Petites resources utiles :
- [pleins d'alias](https://gitlab.com/clovis-ai/dotfiles/-/blob/master/gitconfig), et [comment les utiliser](https://gitlab.com/clovis-ai/dotfiles/-/blob/master/git-aliases.md)
- [un dépôt](https://gitlab.com/clovis-ai/dotfiles) qui contient énormément de scripts pour faciliter l'utilisation de Git (`git ci` pour générer le message de commit suivant les conventions, `git glog` pour afficher l'historique de manière plus lisible, `git autofetch` pour exécuter un `git fetch` automatiquement toutes les 15 minutes, un prompt qui affiche la branche et les commits en cours, `git changelog` pour interpréter les commits de la convention, etc).
- [Un plugin](https://plugins.jetbrains.com/plugin/13477-git-commit-message-helper)  pour les IDE JetBrains pour générer le message de commit
- [Un plugin](https://plugins.jetbrains.com/plugin/index?xmlId=zielu.gittoolbox) pour les IDE JetBrains qui permet de fetch automatiquement (et d'autres choses utiles)
