/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

const {app, globalShortcut, BrowserWindow, ipcMain} = require('electron')

let isDev = false
let devPort

try {
	require('dotenv').config()

} catch (e) {
}


if (process.env.IS_DEV) {
	isDev = parseInt(process.env.IS_DEV)
	devPort = parseInt(process.env.VITE_PORT)
}

const prodURL = `file://${__dirname}//dist`


/**
 * @type mainWindow {Electron.BrowserWindow}
 */
let mainWindow

/**
 * Any other window to be closed when `mainWindow` is closed or reloaded
 * @type {Map<string,Electron.BrowserWindow>}
 */
const auxiliaryWindows = new Map()

/**
 * Creates control panel window and loading its HTML
 */
function createControlWindow() {
	mainWindow = new BrowserWindow({
		autoHideMenuBar: true,
		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule: true,
			contextIsolation: false,
		},
	})

	mainWindow.maximize()
	const url = isDev ? `http://localhost:${devPort}/control/yapovo-electron` : prodURL
	mainWindow.loadURL(`${url}//control_panel.html`).catch((e) => {
		console.error(e)
	})

	mainWindow.on("close", () => {
		mainWindow.webContents.send("stop-yapovo-server")
	})

	mainWindow.on("closed", () => {
		mainWindow = null
	})

	/**
	 * When the yapovo-server is stopped, reloads the page
	 */
	ipcMain.on("yapovo-server-stopped", () => {
		// If the main window is not closed already
		if (mainWindow != null)
			mainWindow.reload()

		closeAuxiliaryWindows()
	})

	/**
	 * Intercepts the event "before-input-event" before its transmission to the renderer process.
	 * If it's a reload action, we call onReload.
	 */
	mainWindow.webContents.on('before-input-event', (event, input) => {
		if ((input.control && input.key.toLowerCase() === 'r') || (input.control && input.alt && input.key.toLowerCase() === 'r')) {
			onReload(event, mainWindow)
		}
	})

	/**
	 * When a refresh is made in the devtools console (ie when the devtools is focused and CTRL+R or CTRL+Shift+R are pressed), a specific event is emitted.
	 * We intercept it and call onReload.
	 */
	mainWindow.webContents.on('devtools-reload-page', (event) => {
		onReload(event, mainWindow)
	})
}

app.on("window-all-closed", function () {
	if (process.platform !== "darwin") {
		app.quit()
	}
})

/**
 * When app is ready, create shortcut for yapovo-server logs and creates the window
 */
app.whenReady().then(() => {
	globalShortcut.register("Alt+CommandOrControl+L", () => {
		mainWindow.webContents.send("show-yapovo-server-log")
	})
}).then(createControlWindow)

/**
 * Prevent reloading and stops the yapovo-server when a reload is asked
 * @param event {Event}
 * @param mainWindow {Electron.BrowserWindow}
 */
function onReload(event, mainWindow) {
	event.preventDefault()
	mainWindow.webContents.send("stop-yapovo-server")

	closeAuxiliaryWindows()
}


// region IPC Button Event Handler

ipcMain.handle("run-yapovo-output", () => {
	const key = "output"
	const url = isDev ? `http://localhost:${devPort}/output/yapovo-output` : prodURL
	createAuxiliaryWindow(key, `${url}//output_panel.html`)
})

ipcMain.handle("run-yapovo-drawing", () => {
	const key = "drawing"
	const url = isDev ? `http://localhost:${devPort}/common/yapovo-drawing` : prodURL
	createAuxiliaryWindow(key, `${url}//index.html`)
})

// endregion


// region Auxiliary Windows

/**
 * Creates an auxiliary window iff there is no open window of
 * key `windowKey`
 * @param windowKey {string} The key to be inserted in `auxiliaryWindows`
 * @param url {string} The HTML URL used by .loadURL()
 */
function createAuxiliaryWindow(windowKey, url) {
	// A window of key `windowKey` is created iff that key is undefined
	// or its value is null
	if (auxiliaryWindows.get(windowKey) != null)
		return

	auxiliaryWindows.set(windowKey, new BrowserWindow({
		autoHideMenuBar: true,
		webPreferences: {
			contextIsolation: false,
			nodeIntegration: true,
			enableRemoteModule: true,
		},
	}))

	auxiliaryWindows.get(windowKey).on("closed", () => {
		auxiliaryWindows.set(windowKey, null)
	})

	auxiliaryWindows.get(windowKey).loadURL(url).catch((e) => {
		console.error(e)
	})
}

function closeAuxiliaryWindows() {
	auxiliaryWindows.forEach((window) => {
		closeWindow(window)
	})
}

/**
 * @param window {Electron.BrowserWindow}
 */
function closeWindow(window) {
	if (window != null) {
		window.close()
	}
}
