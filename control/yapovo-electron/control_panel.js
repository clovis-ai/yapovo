/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {redirectOutput} from "./functions"
import Message from "yapovo-communication"
import Client from "yapovo-client"
import Layout from "yapovo-layout"
import WindowSlot from "yapovo-layout/WindowSlot"
import {LayoutTransfer} from "yapovo-layout/LayoutTransfer"
import {applyHTMLTransformations} from 'yapovo-layout/DragDrop'
import {OutputTransfer} from "yapovo-output/OutputTransfer"

const {desktopCapturer} = require("electron")
export let client
let oldSources = []

/**
 * Spawning a node process from electron binary so that the user does not need to install Node separately.
 * Ths node process runs the file node_process.js.
 */
const {ipcRenderer} = require("electron"),
	app = require('electron').remote.app,
	is_dev = require('electron').remote.process.env.IS_DEV,
	node = require("child_process").fork(
		is_dev ? `${app.getAppPath()}/../../node_modules/yapovo-nodejs/node_process.js` : `${app.getAppPath()}/dist/node_process.js`,
		[],
		{
			stdio: ["pipe", "pipe", "pipe", "ipc"],
		}),
	serverLog = document.querySelector("#serverLog"),
	App = document.querySelector("#app")

console.log(node)

window.addEventListener("beforeunload", () => {
	node.kill("SIGINT")
})

node.on('message', m => {
	const msg = Message.deserialize(m)
	if (msg.type === Message.Type.IPC && msg.payload === "server_connected") {
		controlPanelConnection()
	}
})

// noinspection JSUnusedLocalSymbols Unused parameters could be used in the future
/**
 * Killing node process when "stop-server" event is received (emitted when windows are closed)
 */
ipcRenderer.on("stop-yapovo-server", (event, data) => {
	node.kill("SIGINT")
	ipcRenderer.send("yapovo-server-stopped")
})

// noinspection JSUnusedLocalSymbols Unused parameters could be used in the future
/**
 * Reacting to main process events (CTRL + ALT + L to show server logs)
 */
ipcRenderer.on("show-yapovo-server-log", (event, data) => {
	if (serverLog.style.display === "none") {
		serverLog.style.display = "block"
		App.classList.add("expressAppHide")
	} else {
		App.classList.remove("expressAppHide")
		serverLog.style.display = "none"
	}
})

/**
 * Redirecting node process output to ServerLog
 */
redirectOutput(node.stdout, serverLog)
redirectOutput(node.stderr, serverLog)

/**
 * Delete the sources no longer available
 * @param sources {any} The available sources
 */
function deleteUnuseSources(sources) {
	// deletedSources = oldSources \ sources
	const deletedSources = oldSources.filter(oldSource =>
		!sources.some(newSource => {
			return newSource.id === oldSource.id
		}))

	for (const deletedSource of deletedSources) {
		// Removes the WindowSlot
		const slot = WindowSlot.get(`windowSlot-${deletedSource.id}`)
		slot.div.remove()
		slot.delete()
	}
}


/**
 * Displays every source (window or screen) in the #window-picker
 * container
 */
function loadWindowPicker() {
	desktopCapturer.getSources({
		types: ['window', 'screen'],
	}).then(async sources => {
		const ignoredWindowNames = [
			"Yapovo — Control Panel",
			"Yapovo — Output Panel",
			"Yapovo — Drawing Panel",
			"yapovo",
		]

		// Removes sources whose name is in `ignoredWindowNames`
		sources = sources.filter(source => !ignoredWindowNames.some(name => source.name === name))

		for (const source of sources) {
			// Creates the div element which contains the window thumbnail & title

			if (!WindowSlot.has("windowSlot-" + source.id)) {
				// Creates the WindowSlot
				new WindowSlot(source, client)
			}
		}

		deleteUnuseSources(sources)
		oldSources = sources
	})
}

document.querySelector("#layout-add").onclick = () => new Layout(client).store()

document.querySelector("#layout-del").onclick = () => {
	if (Layout.size() > 1 && confirm(`Are you sure to delete the layout ${Layout.current()} ?`))
		Layout.remove()
}

const windowPickerRefreshTimeout = 500

/**
 *
 * Opening WS connection and authenticating it as a control panel
 */
function controlPanelConnection() {
	client = new Client("127.0.0.1", 4000)
	new LayoutTransfer(client).startEmission()
	new OutputTransfer(client).startTransfer()
	client.onOpen(() => {
		client.authenticateControl(0)
		setInterval(() => loadWindowPicker(), windowPickerRefreshTimeout)
		applyHTMLTransformations()
		new Layout(client).store()
	})
}

// region IPC Buttons

addInvokeEventToElement("#run-output", "run-yapovo-output")
addInvokeEventToElement("#run-drawing", "run-yapovo-drawing")

/**
 * Invokes IPC message `channel` when the element that matches `selector`
 * is clicked.
 * @param selector {string} The HTML element to be selected
 * @param channel {string} The IPC message that is sent to Electron's main process
 */
function addInvokeEventToElement(selector, channel) {
	document.querySelector(selector).addEventListener("click",
		() => ipcRenderer.invoke(channel))
}

// endregion
