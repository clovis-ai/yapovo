/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"
import WindowSlot from "yapovo-layout/WindowSlot.js"

/**
 * Captures an application window or a screen which is printed
 * on the #layout canvas and sent through the WebSocket interface
 */
export default class ScreenSharing {

	/**
	 * The WindowSlot id associated
	 * @type String
	 * @private
	 */
	slotId

	/**
	 * The Electron Source id associated
	 * @type String
	 * @private
	 */
	sourceId

	/**
	 * The HTML video element in which to display the stream
	 * @type HTMLVideoElement
	 * @private
	 */
	video

	/**
	 * The Client that has to receive the stream
	 * @type Client
	 * @private
	 */
	client

	/**
	 * The thumbnal size
	 * @type Object
	 * @private
	 */
	thumbnailSize

	/**
	 * The video fps
	 * @type int
	 * @private
	 */
	fps

	/**
	 * The trackId which is associated to the Slot
	 * @type string
	 */
	trackId

	/**
	 * Construct a ScreenSharing
	 * @param slotId {String}
	 * @param sourceId {String}
	 * @param video {HTMLVideoElement}
	 * @param client {Client}
	 * @param thumbnailSize {{width: number, height: number}}
	 * @param fps {int}
	 */
	constructor(slotId, sourceId, video, client, thumbnailSize, fps) {
		if (client == null)
			throw new Error("Client must not be null")

		this.slotId = slotId
		this.sourceId = sourceId
		this.video = video
		this.client = client
		this.thumbnailSize = thumbnailSize
		this.fps = fps
	}

	/**
	 * Captures the selected application or screen.
	 */
	async start() {
		try {
			if (!this.video.srcObject) {
				const stream = await navigator.mediaDevices.getUserMedia({
					audio: false,
					video: {
						mandatory: {
							chromeMediaSource: 'desktop',
							chromeMediaSourceId: this.sourceId,
						},
					},
				})
				this.handleStream(stream)
			}
		} catch (e) {
			console.error(e)
		}
	}

	/**
	 * Stops the screen capture
	 */
	stop() {
		if (this.video.srcObject) {
			const tracks = this.video.srcObject.getTracks()

			tracks.forEach((track) => {
				track.stop()
			})

			this.client.send(new Message(Message.Type.REMOVE_TRACK, {
				id: 0,
			}))
			this.video.srcObject = null
		}
	}

	/**
	 * Displays the stream in video tag, sends the stream message to Client
	 * Sends **intentionally** one track and replaces it when the user clicks on another window, for WebRTC testing purposes.
	 * @param stream {MediaStream} The application video stream
	 */
	handleStream(stream) {
		if (this.video.srcObject)
			this.stop(this.video.srcObject)
		this.video.srcObject = stream
		this.video.play().then(() => {
			const slot = WindowSlot.get(this.slotId)
			slot.width = this.video.offsetWidth
			slot.height = this.video.offsetHeight
			const track = stream.getTracks()[0] // Temporarily, getting first track
			this.trackId = track.id
			track.applyConstraints({
				frameRate: this.fps,
			}).then(() => {
				WindowSlot.setTrack(this.trackId, {
					track: track,
					stream: stream,
				})
			})
		})
	}
}
