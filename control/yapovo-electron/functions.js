/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

/**
 *
 * Deletes colors from strings
 * @param s
 * @returns {string}
 */
export function strip(s) {
	return s.replace(
		/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
		"",
	)
}

/**
 * Redirects yapovo-output from stdout or stderr to HTMl DOM
 * @param x
 * @param serverLog {Element}
 */
export function redirectOutput(x, serverLog) {
	let lineBuffer = ""

	x.on("data", function (data) {
		lineBuffer += data.toString()
		let lines = lineBuffer.split("\n")

		lines.forEach(l => {
			if (l !== "") {
				serverLog.append(strip(l) + "<br/>")
			}
		})

		lineBuffer = lines[lines.length - 1]
	})
}

/**
 * Adds message data tp reception HTML element
 * @param event {MessageEvent}
 */
export function onMessage(event) {
	document.querySelector("#reception").innerHTML += `</br> ${event.data}`
}
