/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"
import {
	createIndividualSlotDiv,
	JSONtoSVG,
	registerResizeObserver,
	registerVideoPlayListener,
	resizeSVG,
} from "yapovo-layout/LayoutUtilities"

export class OutputTransfer {

	/**
	 * Client for Ws and WebRTC
	 * @type {Client}
	 */
	client

	/**
	 * A map of all registered WindowSlots
	 * @type {Map<String, Object>}
	 */
	windowSlots = new Map()

	/**
	 * Client for Ws and WebRTC
	 * @param client {Client}
	 */
	constructor(client) {
		this.client = client
	}

	/**
	 * Starts the transfer from control panel to output panel
	 */
	startTransfer() {
		this.client.onReception(Message.Type.SVG, this.onSVGReception.bind(this))
	}

	/**
	 * Starts the reception
	 */
	startReception() {
		this.client.onReception(Message.Type.TRACK, this.onTrackReception.bind(this))
		this.client.onReception(Message.Type.SVG, this.onSVGReceptionOutput.bind(this))
		this.client.onReception(Message.Type.LAYOUT, this.onLayoutReception.bind(this))
		this.client.onReception(Message.Type.WINDOW_SLOT, this.onWindowSlotReception.bind(this))
	}

	/**
	 * Callback for SVG reception (control panel)
	 * @param type {int}
	 * @param payload {Object}
	 */
	onSVGReception({type, payload}) {
		this.client.send(new Message(type, payload))
	}

	/**
	 * Callback for SVG reception (output panel)
	 * @param payload {Object}
	 */
	onSVGReceptionOutput({payload}) {
		const slotId = payload.slotId
		const svg = JSONtoSVG(payload.svg)
		const slotSvg = this.windowSlots.get(slotId)?.div?.querySelector('svg')
		if (slotSvg) {
			if (!slotSvg.getAttribute("viewBox")) {
				slotSvg.setAttribute("viewBox", payload.viewBox)
			}
			slotSvg.appendChild(svg)
		}
	}

	/**
	 * Callback for track reception
	 * @param msg {Object}
	 */
	onTrackReception(msg) {

		let windowSlot = null

		this.windowSlots.forEach(slot => {
			if (slot.trackId === msg.track.id) {
				windowSlot = slot
			}
		})

		if (windowSlot) {
			const {individualDiv, svg, video} = createIndividualSlotDiv(msg.streams[0])
			this.resizeVideo(individualDiv, windowSlot)
			windowSlot.div = individualDiv
			registerResizeObserver(svg, video)
			registerVideoPlayListener(svg, video)
			this.registerVideoResize(individualDiv, windowSlot)
			document.querySelector('#layout').appendChild(individualDiv)
			video.play().then(() => {
				resizeSVG(svg, video)
			})
		}
	}

	/**
	 * Callback for windowSlot reception
	 * @param msg {Object}
	 */
	onWindowSlotReception(msg) {
		msg.payload.slots.forEach(slot => {
			if (this.windowSlots.get(slot.slotId)) {
				this.windowSlots.get(slot.slotId).trackId = slot.trackId
			}
		})
	}

	/**
	 * Callback for layout reception
	 * @param msg {Object}
	 */
	onLayoutReception(msg) {
		msg.payload.windowSlotPositions.forEach(slot => {
			const oldSlot = this.windowSlots.get(slot.slotId)
			if (oldSlot) {
				this.updateSlot(oldSlot, slot)
				if (oldSlot.div) {
					this.resizeVideo(oldSlot.div, slot)
				}
			} else {
				slot.isViewboxSet = false
				this.windowSlots.set(slot.slotId, slot)
			}
		})
	}

	/**
	 * Updates a slot specs
	 * @param slot {Object}
	 * @param newSlotSpecs {Object}
	 */
	updateSlot(slot, newSlotSpecs) {
		slot.x = newSlotSpecs.x
		slot.y = newSlotSpecs.y
		slot.width = newSlotSpecs.width
		slot.height = newSlotSpecs.height
	}

	/**
	 * Resizes the video
	 * @param div {HTMLElement}
	 * @param slot {Object}
	 */
	resizeVideo(div, slot) {
		const video = div.querySelector('video')
		if (video) {
			video.style.width = `${slot.width * window.innerWidth}px`
			video.style.height = `${slot.height * window.innerHeight}px`
			div.style.top = `${slot.y * window.innerHeight}px`
			div.style.left = `${slot.x * innerWidth}px`
		}
	}

	/**
	 * Registers a callback when video is resized
	 * @param div {HTMLElement}
	 * @param slot {Object}
	 */
	registerVideoResize(div, slot) {
		window.addEventListener("resize", () => {
			this.resizeVideo(div, slot)
		})
	}
}

