# Yapovo PFA ENSEIRB 2020-2021

A tool for teachers and instructors to annotate their screen, with remote capabilities (it's
possible to annotate from another device than the one sharing the screen).

To contribute, please read: [CONTRIBUTING.md](CONTRIBUTING.md).

Download the latest version:

- [MacOS (Darwin) x64](https://clovis-ai.gitlab.io/yapovo/yapovo-darwin-x64.zip)
- [Linux x64](https://clovis-ai.gitlab.io/yapovo/yapovo-linux-x64.zip)

## Development environment

You will need to install `node` and `yarn` using your preferred method. The development commands
needed are:

- `yarn install`: Installs all dependencies
- `yarn run vite`: Starts the development server
- `yarn run electron`: Starts Yapovo (the development server must be running)
- `yarn run test`: Executes unit tests
- `yarn run coverage`: Calculates code coverage

If you are using an IDE from JetBrains, you will find these commands as run configurations.

To export the project, use:

- `yarn run package:<platform>` where `<platform>` is one of `linux`, `darwin` or `windows`.
