/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import getMousePosition, {scaleToViewbox} from "./Tool"

/**
 * A laser pointer tool that follows the cursor
 */
export default class LaserPointer {

	static pointerRadius = "8"

	static instance = new LaserPointer()

	constructor() {
		this.isClicked = false
		this.circleInit()
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Updates this tool.
	 * @param {String} type The name of the event that leads to updating this tool.
	 * @param {MouseEvent} event The event that leads to updating this tool.
	 * @param {Element} windowSlotSVG
	 * @param video
	 */
	update(type, event, windowSlotSVG, video) {
		switch (type) {
			case "mousemove":
				this.draw(event, windowSlotSVG, video)
				break
			case "mouseout":
				windowSlotSVG.removeChild(this.pointer)
				break
		}
	}

	/**
	 * Private method to initialize the circle element
	 * when the constructor is called.
	 */
	circleInit() {
		// Create and init attributes of the circle element
		this.pointer = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
		this.pointer.setAttribute("fill", "red")
		this.pointer.setAttribute("class", "annotation")
		this.pointer.setAttribute("opacity", "0.6")
		this.pointer.setAttribute("r", LaserPointer.pointerRadius)
	}

	/**
	 * Private method to draw the pointer at the cursor position.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG
	 * @param video
	 */
	draw(event, windowSlotSVG, video) {
		if (this.pointer) {
			const position = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
			this.pointer.setAttribute("cx", position.x)
			this.pointer.setAttribute("cy", position.y)
			windowSlotSVG.appendChild(this.pointer)
		}
	}
}
