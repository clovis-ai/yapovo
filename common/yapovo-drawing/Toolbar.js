/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import FreeHandTool from "./FreeHandTool"
import Mouse from "./Mouse"
import Line from "./Line"
import Rectangle from "./Rectangle"
import LaserPointer from "./LaserPointer"
import EraserTool from "./EraserTool"
import Client from "yapovo-client"
import Message from "yapovo-communication"
import {properties} from "./Tool"
import {LayoutTransfer} from "yapovo-layout/LayoutTransfer"
import ClientWindowSlot from "./ClientWindowSlot"
import {parse} from "svgson"
import {registerResizeObserver, resizeSVG} from "yapovo-layout/LayoutUtilities"

/**
 *
 * @class Toolbar
 * @classdesc Creates the toolbar and init events on the different SVG.
 */
export default class Toolbar {

	/**
	 * The list of available tools.
	 *
	 * Each tool must have a `update(type, event)` method such that:
	 * - `type` is a string representing the name of the event ("mouseup", …),
	 * - `event` is the Event that was fired.
	 *
	 * @type {{FreeHandTool: FreeHandTool, Line: Line, Mouse: Mouse, Rectangle: Rectangle}}
	 */
	tools = {
		Mouse: Mouse.instance,
		FreeHandTool: FreeHandTool.instance,
		Line: Line.instance,
		Rectangle: Rectangle.instance,
		EraserTool: EraserTool.instance,
		LaserPointer: LaserPointer.instance,
	}

	/**
	 * The tool currently selected by the user.
	 * By default the selected tool is Mouse.
	 */
	currentTool = this.tools.Mouse

	/**
	 * The list of ids of each available tool.
	 *
	 * Each tool as an id which correspond to the id of a button in the
	 * HTML representation of a toolbar.
	 *
	 * @type {{FreeHandTool: string, Line: string, Mouse: string, Rectangle: string}}
	 */
	toolIds = {
		Mouse: "#mouse-button",
		FreeHandTool: "#free-button",
		Line: "#line-button",
		Rectangle: "#rectangle-button",
		EraserTool: "#eraser-button",
		LaserPointer: "#laser-pointer",
	}

	/**
	 * The list of ids of each available thickness tool.
	 *
	 * Each tool as an id which correspond to the id of a button in the
	 * HTML representation of a sub-toolbar.
	 *
	 * @type {{Small: string, XLarge: string, Medium: string, Large: string}}
	 */
	thicknessToolIds = {
		Small: "#small-button",
		Medium: "#medium-button",
		Large: "#large-button",
		XLarge: "#xlarge-button",
	}

	/**
	 * The list of ids of each available property.
	 *
	 * Each property as an id which correspond to the id of a button in the
	 * HTML representation of a toolbar.
	 *
	 * @type {{Thickness: string}}
	 */
	propertyToolIds = {
		Thickness: "#thickness-button",
	}

	/**
	 * The id of the tool currently selected.
	 * By default the selected tool is Mouse.
	 *
	 * @type {string}
	 */
	currentToolId = this.toolIds.Mouse

	/**
	 * The id of the thickness tool currently selected.
	 * By default the selected tool is Small.
	 *
	 * @type {string}
	 */
	currentThicknessToolId = this.thicknessToolIds.Small

	/**
	 * The id of the thickness property tool currently selected. (tool that open sub-menu)
	 * By default the tool is not selected
	 */
	currentThicknessPropertyId = null

	currentLayout

	constructor() {
		this.socket = new Client("localhost", 4000)

		this.socket.onOpen(() => {
			this.socket.authenticateClient()
		})

		this.layoutTransfer = new LayoutTransfer(this.socket)
		this.layoutTransfer.startReception(this.onLayoutReception.bind(this), this.onWindowSlotReception.bind(this), this.onTrackReception.bind(this))

		// Activate the different tools actions.
		this.globalToolChange(this.toolIds, this.currentToolId, "active", this)
		this.globalToolChange(this.thicknessToolIds, this.currentThicknessToolId, "sub-active", null)
		this.globalToolChange(this.propertyToolIds, this.currentThicknessPropertyId, "property-active", null)
		this.subMenuClosed(this.propertyToolIds)
	}

	/**
	 * Private method that close all sub-menu property if focus is changes
	 * except in drawing area the toolbar and the sub-menu itself.
	 *
	 * @param {Object} ids The list of ids considered
	 */
	subMenuClosed(ids) {
		document.addEventListener("click", function (event) {
			const subMenu = document.querySelector(".sub-icon-bar")
			const toolbar = document.querySelector(".icon-bar")
			const drawingArea = document.querySelector(".toolbar-div")
			const isClickedInside = event.target instanceof Node && (toolbar.contains(event.target) || subMenu.contains(event.target) || drawingArea.contains(event.target))
			if (!isClickedInside) {
				Object.keys(ids).forEach((toolName) => {
					const button = document.querySelector(ids[toolName])
					button.classList.remove("property-active")
					subMenu.style.display = "none"
				})
			}
		})
	}

	/**
	 * Tool change manager :
	 * - Change current tool depending on the button clicked and if the tool is not a property tool.
	 * - Change css class depending on which tool or property tool is already activated.
	 * - Apply the right thickness property
	 *
	 * @param {Object} ids The list of ids considered
	 * @param {String} currentId The id of the current tool considered (drawing tool, property)
	 * @param {String} className The specific class to apply on the button tool
	 * @param {Toolbar} toolbar The instance of the toolbar
	 */
	globalToolChange(ids, currentId, className, toolbar) {
		Object.keys(ids).forEach((toolName) => {

			// Button clicked
			const button = document.querySelector(ids[toolName])

			button.addEventListener("click", () => {

				// Currently activated button
				const activatedButton = document.querySelector(currentId)

				/**
				 * Activated button (is not null) and clicked button are different :
				 * - remove css class to activatedButton which indicate the activated button.
				 * - add css class to clicked button which indicate activated button.
				 */
				if (className !== "property-active") {
					activatedButton.classList.remove(className)
					button.classList.add(className)
					currentId = ids[toolName]
				} else {
					/**
					 * Buttons relative to the property of the tool
					 * are set with a on/off behaviour.
					 */
					const parent = button.parentNode
					const subMenu = parent.querySelector('.sub-icon-bar')

					if (button.classList.contains("property-active")) {
						activatedButton.classList.remove(className)
						subMenu.style.display = "none"
					} else {
						button.classList.add(className)
						subMenu.style.display = "block"
						currentId = ids[toolName]
					}
					return
				}

				/**
				 * If toolbar is given, then the tool is not a property tool
				 * - tool related to the clicked button is now the current tool.
				 * - otherwise, the actual tool is a property tool
				 */
				if (toolbar !== null) {
					toolbar.currentTool = toolbar.tools[toolName]
				} else {
					properties.thickness = Number(button.value)
				}
			})
		})
	}

	/**
	 * Add the different eventListeners to the SVG element given in parameter.
	 *
	 * @param {SVGElement} windowSlotSVG
	 * @param {String} slotId
	 * @param video
	 */
	createEventListener(windowSlotSVG, slotId, video) {
		const capturedEvents = ["mouseup", "mousedown", "mouseout", "mousemove"]

		capturedEvents.forEach((eventName) => {
			// For example, on 'mousemove', call `currentTool.update("mousemove", event)` (with added null checking & binding)
			windowSlotSVG.addEventListener(
				eventName,
				(event) => {
					const svg = this.currentTool?.update?.bind(this.currentTool)?.(eventName, event, windowSlotSVG, video)
					if (svg) {
						const svgElement = document.createElement('svg')
						svgElement.appendChild(svg.cloneNode(true))
						parse(svgElement.outerHTML).then(json => {
							const msg = new Message(Message.Type.SVG, {
								"slotId": slotId,
								"svg": json,
								"viewBox": `0 0 ${video.offsetWidth} ${video.offsetHeight}`,
							})
							this.socket.send(msg)
						})
					}
				},
			)
		})
	}

	/**
	 * Callback methods when a Layout is received.
	 * @param layouts {Map<String, Layout>}
	 * @param layoutReceived {Object}
	 * @param oldSlot {Map<String, Layout>}
	 */
	onLayoutReception(layouts, layoutReceived, oldSlot) {
		this.currentLayout = layoutReceived
		this.cleanSidebar()
		this.updateDrawingPanel(oldSlot)
	}

	/**
	 * Callback methods when a window slot is received.
	 * @param windowSlots {Map<String, WindowSlot>}
	 * @param windowSlotsReceived {Object}
	 */
	onWindowSlotReception(windowSlots, windowSlotsReceived) {
		this.updateDrawingPanel(windowSlotsReceived)
	}

	/**
	 * Reinitialize the sidebar.
	 */
	cleanSidebar() {
		const divSelector = document.querySelector("#selector")
		divSelector.innerHTML = ""
	}

	/**
	 * Update the drawing panel sidebar
	 * @param receivedElements {Map <String, WindowSlot> | Object}
	 */
	updateDrawingPanel(receivedElements) {
		const divSelector = document.querySelector("#selector")
		receivedElements.forEach((element) => {
			this.createRadioButton(element, divSelector)
		})
	}

	/**
	 * Create a radio button in the sidebar. If the element is a layout the radio-button is
	 * checked.
	 * @param element {Object}
	 * @param parentDiv {Element}
	 */
	createRadioButton(element, parentDiv) {
		let divRadioButtonLabel = document.createElement("div")
		divRadioButtonLabel.setAttribute("class", "one-radio")

		let radioButton = document.createElement("input")
		radioButton.setAttribute("type", "radio")
		radioButton.setAttribute("name", "windowSelector")
		radioButton.setAttribute("class", "radio-button")
		radioButton.setAttribute("id", element.trackId)

		radioButton.addEventListener("click", this.callbackWindowSlotSelected.bind(this))

		let label = document.createElement("label")
		label.setAttribute("for", element.trackId)
		label.innerHTML = element.title

		divRadioButtonLabel.appendChild(radioButton)
		divRadioButtonLabel.appendChild(label)

		parentDiv.appendChild(divRadioButtonLabel)
	}

	/**
	 * Callback methods when a track is received.
	 * @param windowSlots {Map<String, WindowSlot>}
	 * @param trackReceived {MediaStreamTrack}
	 * @param streamReceived {MediaStream}
	 */
	onTrackReception(windowSlots, trackReceived, streamReceived) {
		let windowSlotPosition = null
		this.currentLayout.windowSlotPositions.forEach(windowSlot => {
			if (windowSlots.get(windowSlot.slotId).trackId === trackReceived.id) {
				windowSlotPosition = windowSlot
			}
		})

		// If the received id doesn't correspond to a WindowSlot in the map.
		if (!windowSlotPosition) {
			console.warn("Received id doesn't match any existing WindowSlot !")
			return
		}

		let concernedWindowSlot = windowSlots.get(windowSlotPosition.slotId)
		let clientWindowSlot = new ClientWindowSlot(windowSlotPosition.slotId, concernedWindowSlot.trackId, this, trackReceived, streamReceived)

		// Get the svg in the WindowSlot annotations.
		const svg = clientWindowSlot.annotations.querySelector("svg")
		const video = clientWindowSlot.annotations.querySelector("video")

		registerResizeObserver(svg, video)
	}

	/**
	 * Function call when a radio button is clicked.
	 * @param event {MouseEvent}
	 */
	callbackWindowSlotSelected(event) {
		let clientWindowSlot = ClientWindowSlot.clientWindowSlots.get(event.target.id)
		let drawingArea = document.querySelector("#drawing-area")
		drawingArea.innerHTML = ""
		drawingArea.appendChild(clientWindowSlot.annotations)
		const video = clientWindowSlot.annotations.querySelector('video')
		const svg = clientWindowSlot.annotations.querySelector('svg')
		if (video) {
			video.play().then(() => {
				resizeSVG(svg, video, clientWindowSlot)
			})
		}
	}
}
