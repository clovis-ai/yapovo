/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

/**
 *
 * @class Mouse
 * @classdesc Creates a Mouse tool (future use to bypass the overlay
 * 			  and interact with the window).
 * @see instance
 */
export default class Mouse {

	static instance = new Mouse()

	/**
	 * Updates this tool.
	 * @param type {string} The name of the event that lead to updating this tool.
	 * @param event {Event} The event that lead to updating this tool.
	 */
	update(type, event) {
		// Nothing to do here currently
	}
}
