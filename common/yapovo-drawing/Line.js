/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import getMousePosition, {properties, scaleToViewbox} from "./Tool"

/**
 * Line
 *
 * @class Line
 * @classdesc Creates a line tool in order to draw
 * 			  in the SVG element with the cursor.
 * @see instance
 */
export default class Line {

	static instance = new Line()

	/*
	* The authorized error in pixel of the cursor position
	* between the pressure and the released of the mouse.
	*
	* It allows to identify if it's a long or a double click.
	*/
	marginError = 7

	constructor() {
		this.isClicked = false
		this.isDoubleClicked = false
		this.line = null
		this.linePreview = null
		this.beginPoint = null
		this.endPoint = null
		this.tmpPoint = null
		this.canSend = false
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Updates this tool.
	 * @param type {string} The name of the event that lead to updating this tool.
	 * @param event {MouseEvent} The event that lead to updating this tool.
	 * @param windowSlotSVG {Element} The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	update(type, event, windowSlotSVG, video) {
		switch (type) {
			case "mousedown":
				this.isClicked = true
				this.lineInit(event, windowSlotSVG, video)
				break
			case "mouseup":
				this.isClicked = false
				if (this.line !== null)
					this.draw(event, windowSlotSVG, video)
				this.tmpPoint = null
				if (this.canSend) {
					this.canSend = false
					return this.line
				}
				break
			case "mouseout":
			case "oncontextmenu":
				if (this.linePreview !== null && this.isClicked === true)
					this.linePreview.remove()
				this.isClicked = false
				this.linePreview = null
				this.isDoubleClicked = false

				this.line = null
				this.tmpPoint = null
				break
			case "mousemove":
				// In case of "long click" configuration a preview of the line is drawn
				// everytime a mousemove is detected.
				if (this.isClicked) {
					if (this.tmpPoint !== null) {
						this.linePreview.setAttribute('x1', this.tmpPoint.x)
						this.linePreview.setAttribute('y1', this.tmpPoint.y)
					}
					this.drawLinePreview(event, windowSlotSVG, video)
				}
				break
		}
	}

	/**
	 * Private method to initialize the line element
	 * at the "mousedown" event.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	lineInit(event, windowSlotSVG, video) {

		if (!this.isDoubleClicked) {
			// Create and init attributes of the line element
			this.line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
			this.line.setAttribute("stroke-width", properties.thickness)
			this.line.setAttribute("class", "annotation")
			this.line.setAttribute("stroke", "black")

			// Add the first point of the line to our SVG
			this.beginPoint = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
			this.line.setAttribute('x1', this.beginPoint.x)
			this.line.setAttribute('y1', this.beginPoint.y)

			// Creates a reference of the line
			this.linePreview = this.line
			this.line.setAttribute("opacity", 0.5)
		} else {

			// We save the position of the mouse in case where the user would draw with a
			// long click instead of double click
			this.tmpPoint = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		}
	}

	/**
	 * Private method to preview line draw on screen.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	drawLinePreview(event, windowSlotSVG, video) {
		const pt = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		this.linePreview.setAttribute('x2', pt.x)
		this.linePreview.setAttribute('y2', pt.y)
		windowSlotSVG.appendChild(this.linePreview)
	}

	/**
	 * Private method to draw the line on screen.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	draw(event, windowSlotSVG, video) {
		this.endPoint = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		if (this.line &&
			this.checkMarginError(this.beginPoint.x, this.beginPoint.y)) {

			this.line.setAttribute('x2', this.endPoint.x)
			this.line.setAttribute('y2', this.endPoint.y)
			this.line.setAttribute("opacity", 1)
			windowSlotSVG.appendChild(this.line)
			this.isDoubleClicked = false
			this.canSend = true
		} else {
			this.isDoubleClicked = true
		}
	}

	/**
	 * Private method to check if the position of the cursor between the pressure and
	 * the released of the mouse click is equal or lesser than the margin error.
	 *
	 * @param x {int} The x position of the cursor.
	 * @param y {int} The y position of the cursor.
	 */
	checkMarginError(x, y) {
		return (Math.abs(this.endPoint.x - x) > this.marginError ||
			(Math.abs(this.endPoint.y - y) > this.marginError))
	}
}
