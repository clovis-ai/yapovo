/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */


/**
 * The of properties applied to the tools.
 */
let properties = {
	thickness: 1,
}

export {properties}

/**
 * Return an object with a value x and y which is the
 * abscissa and the ordinate of the mouse.
 *
 * @param {MouseEvent} event
 * @param {Element} svgElement
 */
export default function getMousePosition(event, svgElement) {
	const rect = svgElement.getBoundingClientRect()
	return {
		x: event.pageX - rect.left,
		y: event.pageY - rect.top,
	}
}

export function scaleToViewbox(svg, video, pt) {
	const viewbox = svg.viewBox.baseVal
	const coeffX = video.offsetWidth / viewbox.width
	const coeffY = video.offsetHeight / viewbox.height
	pt.x = pt.x / coeffX
	pt.y = pt.y / coeffY
	return pt
}
