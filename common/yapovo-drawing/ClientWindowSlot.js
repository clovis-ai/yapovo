/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */


import {
	createIndividualSlotDiv,
	registerVideoPlayListener,
	resizeSVG,
} from "yapovo-layout/LayoutUtilities"

/**
 * A WindowSlot corresponds to a captured
 * window and its specific annotation. A
 * WindowSlot can be identified by its id.
 */
export default class ClientWindowSlot {

	/**
	 * Map of every existing WindowSlot.
	 * @type Map<String, ClientWindowSlot>
	 */
	static clientWindowSlots = new Map()

	/**
	 * The trackId associated to the ClientWindowSlot
	 * @type {string}
	 */
	trackId

	/**
	 * Annotations div of the slot
	 * @type {Node}
	 */
	annotations

	/**
	 * Tells if a viewbox has already been set on the SVG
	 * @type {boolean}
	 */
	isViewboxSet = false

	/**
	 *
	 * @param slotId {String} The ID of the slot
	 * @param trackId {String} The ID of the track
	 * @param toolbar {Toolbar}
	 * @param track {MediaStreamTrack}
	 * @param stream {MediaStream}
	 */
	constructor(slotId, trackId, toolbar, track, stream) {
		// If the ClientWindowSlot is not already in the class map
		if (!ClientWindowSlot.clientWindowSlots.has(trackId)) {

			/**
			 * The identifier of the WindowSlot but also the key of the global
			 * map.
			 */
			this.trackId = trackId

			const {individualDiv: annotations, svg, video} = createIndividualSlotDiv(stream)
			this.annotations = annotations

			// If a toolbar exists, we add the different eventListener
			toolbar?.createEventListener(svg, slotId, video)

			// Add this WindowSlot to the global map.
			ClientWindowSlot.clientWindowSlots.set(trackId, this)

			registerVideoPlayListener(svg, video)

			/**
			 * Add an eventListener to resize the SVG when
			 * the drawing panel is resized.
			 */
			window.addEventListener("resize", () => {
				resizeSVG(svg, video)
			})
		}
	}
}
