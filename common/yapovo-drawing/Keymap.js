/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {shortcuts} from './config'

/**
 * Binds the Keyboard shortcuts to their tools
 * @param toolbar
 */
export default function setKeymap(toolbar) {
	document.addEventListener("keydown", (event) => {
		if (!toolbar.currentTool.isClicked)
			toggleTool(shortcuts[event.key.toLowerCase()])
	})
}

/**
 * Simulates a click event on the button corresponding to the tool
 * @param toolId
 */
function toggleTool(toolId) {
	const button = document.querySelector(toolId)
	button?.click()
}
