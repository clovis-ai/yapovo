/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import getMousePosition, {properties, scaleToViewbox} from "./Tool"
import PathAnnotation from "./PathAnnotation"

/**
 * FreeHandTool
 *
 * @class FreeHandTool
 * @classdesc Creates a freehand yapovo-drawing tool in order to draw
 * 			  in the SVG element with the cursor.
 * @see instance
 */
export default class FreeHandTool {

	static instance = new FreeHandTool()

	constructor() {
		this.isClicked = false
		// The variable that will contain a PathAnnotation instance
		this.path = null

		// The svg element that we will create
		this.svg = null
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Updates this tool.
	 * @param {String} type The name of the event that lead to updating this tool.
	 * @param {MouseEvent} event The event that lead to updating this tool.
	 * @param {SVGElement} windowSlotSVG
	 * @param video
	 */
	update(type, event, windowSlotSVG, video) {
		switch (type) {
			case "mousedown":
				this.isClicked = true
				this.pathInit(event, windowSlotSVG, video)
				break
			case "mouseup":
			case "mouseout":
				this.isClicked = false
				if (this.path) {
					// save svg element in PathAnnotation instance
					this.path.svg = this.svg
					// add the PathAnnotation instance to the global mapping pathAnnotations
					this.path.savePath(windowSlotSVG)
					const tmp = this.svg
					this.svg = null
					this.path = null
					return tmp
				}
				break
			case "mousemove":
				this.draw(event, windowSlotSVG, video)
				break
		}
	}

	/**
	 * Private method to initialize the path element
	 * at the first "mousedown" event.
	 *
	 * @param {MouseEvent} event
	 * @param {SVGElement} windowSlotSVG
	 * @param video
	 */
	pathInit(event, windowSlotSVG, video) {

		// Create and init attributes of the path element
		this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'path')
		this.svg.setAttribute("fill", "none")
		this.svg.setAttribute("stroke", "red")
		this.svg.setAttribute("stroke-width", properties.thickness)
		this.svg.setAttribute("class", "annotation")

		this.path = new PathAnnotation()
		// Add the first path element to our main SVG
		const pt = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		this.path.data.push([pt.x, pt.y])

		this.svg.setAttribute("d", this.path.svgPath())
		windowSlotSVG.appendChild(this.svg)
	}

	/**
	 * Private method to draw on screen.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG
	 * @param video
	 */
	draw(event, windowSlotSVG, video) {
		if (this.path) {
			const pt = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
			this.path.data.push([pt.x, pt.y])
			this.svg.setAttribute('d', this.path.svgPath())
		}
	}
}
