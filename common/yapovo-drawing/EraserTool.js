/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import PathAnnotation, {pathAnnotations} from "./PathAnnotation"
import getMousePosition, {properties, scaleToViewbox} from "./Tool"
import erase from "@serrabii/svg-erase"

/**
 * EraserTool
 *
 * @class EraserTool
 * @classdesc Creates an eraser tool in order to erase
 * svg annotations of type path.
 */
export default class EraserTool {

	static instance = new EraserTool()

	constructor() {
		this.isClicked = false
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Updates this tool.
	 *
	 * @param {string} type - the type of the mouse event
	 * @param {MouseEvent} event - the mouse event
	 * @param {Element} windowSlotSVG - The SVG element that detected the event
	 * @param video
	 */
	update(type, event, windowSlotSVG, video) {
		switch (type) {
			case "mousedown":
				this.isClicked = true
				// eraser thickness must always be bigger than path annotations
				this.eraserRadius = properties.thickness + 20
				this.erasePathInit(event, windowSlotSVG, video)
				break
			case "mouseup":
			case "mouseout":
				if (this.isClicked) {
					//remove trace of the eraser
					windowSlotSVG.removeChild(this.svg)

					//remove paths in erase path
					if (pathAnnotations.has(windowSlotSVG))
						this.updatePaths(windowSlotSVG)
				}
				this.erasePath = null
				this.isClicked = false
				break
			case "mousemove":
				if (this.isClicked)
					this.draw(event, windowSlotSVG, video)
				break
		}
	}

	/**
	 * Initializes the svg element and creates a new PathAnnotation instance
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG
	 * @param video
	 */
	erasePathInit(event, windowSlotSVG, video) {
		this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'path')
		this.svg.setAttribute('fill', 'none')
		this.svg.setAttribute('stroke', 'gray')
		this.svg.setAttribute('opacity', `${0.3}`)
		this.svg.setAttribute('stroke-width', `${this.eraserRadius * 2}`)
		this.svg.setAttribute('stroke-linecap', 'round')
		this.svg.setAttribute('stroke-linejoin', 'round')
		this.svg.classList.add('annotation')

		this.erasePath = new PathAnnotation()
		const mousePos = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		this.erasePath.data.push([mousePos.x, mousePos.y])

		this.svg.setAttribute('d', this.erasePath.svgPath() + ' Z')
		windowSlotSVG.appendChild(this.svg)
	}

	/**
	 * Draws on windowSlotSVG
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG
	 * @param video
	 */
	draw(event, windowSlotSVG, video) {
		if (this.erasePath) {
			const mousePos = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
			this.erasePath.data.push([mousePos.x, mousePos.y])
			this.svg.setAttribute('d', this.erasePath.svgPath())
		}
	}

	/**
	 * Updates the path annotations of windowSlotSVG
	 *
	 * @param {Element} windowSlotSVG
	 */
	updatePaths(windowSlotSVG) {
		// update pathAnnotations
		let paths = pathAnnotations.get(windowSlotSVG)
		let data = paths.map(path => path.data)
		let [newData, indices] = erase(data, this.erasePath.data, this.eraserRadius)
		let newPaths = newData.map((data, i) => {
			let path = new PathAnnotation(data)
			path.setSVG(paths[indices[i]].svg)
			path.svg.setAttribute('d', path.svgPath())
			return path
		})
		pathAnnotations.set(windowSlotSVG, newPaths)

		// clear old path annotations from windowSlotSVG
		Array.from(windowSlotSVG.childNodes)
			.filter(node => node.tagName === 'path')
			.forEach(windowSlotSVG.removeChild.bind(windowSlotSVG))

		// append new path annotations to windowSlotSVG
		for (const pathAnnot of pathAnnotations.get(windowSlotSVG))
			windowSlotSVG.appendChild(pathAnnot.svg)
	}
}
