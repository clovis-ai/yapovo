/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import getMousePosition, {properties, scaleToViewbox} from "./Tool"

/**
 * Rectangle
 *
 * @class Rectangle
 * @classdesc Creates a rectangle tool in order to draw
 * 			  in the SVG element with the cursor.
 * @see instance
 */
export default class Rectangle {

	static instance = new Rectangle()

	constructor() {
		this.isClicked = false
		this.rect = null
		this.rectPreview = null
		this.beginPoint = 0
		this.fixedBeginPoint = 0
		this.endPoint = 0
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Updates this tool.
	 * @param {string} type The name of the event that lead to updating this tool.
	 * @param {MouseEvent} event The event that lead to updating this tool.
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	update(type, event, windowSlotSVG, video) {
		switch (type) {
			case "mousedown":
				this.isClicked = true
				this.rectInit(event, windowSlotSVG, video)
				break
			case "mouseup":
				this.draw(event, windowSlotSVG, video)
				this.isClicked = false
				this.rectPreview = null
				return this.rect
			case "mouseout":
				if (this.rectPreview !== null && this.isClicked === true)
					windowSlotSVG.removeChild(this.rectPreview)
				this.isClicked = false
				this.rectPreview = null
				this.rect = null
				break
			case "mousemove":
				if (this.isClicked) {
					this.drawRectPreview(event, windowSlotSVG, video)
				}
		}
	}

	/**
	 * Private method to preview rectangle draw on screen.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	drawRectPreview(event, windowSlotSVG, video) {
		this.setRectangleAttribute(this.rectPreview, event, windowSlotSVG, video)
		windowSlotSVG.appendChild(this.rectPreview)
	}

	/**
	 * Private method to initialize the rectangle element
	 * at the "mousedown" event.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	rectInit(event, windowSlotSVG, video) {

		// Create and init attributes of the rect element
		this.rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
		this.rect.setAttribute("fill", "none")
		this.rect.setAttribute("stroke", "black")
		this.rect.setAttribute("stroke-width", properties.thickness)
		this.rect.classList.add("annotation")

		// Save the first point of the rectangle
		this.beginPoint = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))

		// Copy values of the rectangle to the rectangle preview
		this.rectPreview = this.rect
		this.rect.setAttribute("opacity", 0.5)
		this.fixedBeginPoint = {...this.beginPoint}
	}

	/**
	 * Private method to draw on screen.
	 *
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	draw(event, windowSlotSVG, video) {
		if (this.rect) {
			// Save the second point of the rectangle
			this.isClicked = false
			this.setRectangleAttribute(this.rect, event, windowSlotSVG, video)
			this.rect.setAttribute("opacity", 1)
			windowSlotSVG.appendChild(this.rect)
		}
	}

	/**
	 * Private method to set positions of the rectangle.
	 *
	 * @param {Element} rect The considered rectangle
	 * @param {MouseEvent} event
	 * @param {Element} windowSlotSVG The SVG element from the WindowSlot which detect the event.
	 * @param video
	 */
	setRectangleAttribute(rect, event, windowSlotSVG, video) {
		this.endPoint = scaleToViewbox(windowSlotSVG, video, getMousePosition(event, windowSlotSVG))
		this.beginPoint = {...this.fixedBeginPoint}
		this.adaptPoint()
		rect.setAttribute('x', this.beginPoint.x)
		rect.setAttribute('y', this.beginPoint.y)
		rect.setAttribute('width', this.endPoint.x - this.beginPoint.x)
		rect.setAttribute('height', this.endPoint.y - this.beginPoint.y)
	}

	/**
	 * Private method to swap two variables' values
	 *
	 * @param {number} x
	 * @param {number} y
	 */
	swap(x, y) {
		return [y, x]
	}

	/**
	 * Private method to adapt point values to constraints
	 * about rectangle creation.
	 */

	/*
		   width
		x,y---►
		| \   |
		|  \  |  height
		|   \ |
		▼-----X
		To create the right rectangle, the x1,y1 values of the first point (mousedown)
		must be lesser than the x2,y2 values of the second point (mouseup).
		Otherwise the width or height of the rectangle would be negative.
		To prevent this behaviour, we switch the value of the abcissa and/or ordinate
		of the first point with those of the second if values of the former are lesser than
		those f the latter.
	 */
	adaptPoint() {
		if (this.beginPoint.x > this.endPoint.x) {
			[this.beginPoint.x, this.endPoint.x] = this.swap(this.beginPoint.x, this.endPoint.x)
		}
		if (this.beginPoint.y > this.endPoint.y) {
			[this.beginPoint.y, this.endPoint.y] = this.swap(this.beginPoint.y, this.endPoint.y)
		}
	}
}
