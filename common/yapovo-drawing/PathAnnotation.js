/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

/**
 * Mapping SVG elements to their
 * path annotations
 */
export const pathAnnotations = new Map()

/**
 * PathAnnotation
 *
 * @class PathAnnotation
 * @classdesc A PathAnnotation defines an annotation
 * of type 'path'. A particular PathAnnotation belongs
 * to an SVG element and has an array of points.
 */
export default class PathAnnotation {

	constructor(path = []) {
		this.data = path
	}

	/**
	 * Adds the data of the new path to pathAnnotations
	 *
	 * @param {Element} windowSlotSVG
	 */
	savePath(windowSlotSVG) {
		let paths = pathAnnotations.get(windowSlotSVG)
		if (paths) {
			paths.push(this)
			pathAnnotations.set(windowSlotSVG, paths)
		} else {
			pathAnnotations.set(windowSlotSVG, [this])
		}
	}

	/**
	 * Set svg member of PathAnnotation by copying properties from anSVG
	 *
	 * @param {Element} anSVG
	 */
	setSVG(anSVG) {
		this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'path')

		Array.from(anSVG.attributes).forEach(attr => {
			this.svg.setAttribute(attr.name, attr.value)
		})
	}

	// noinspection JSValidateTypes
	/**
	 * Transforms array of points (this.data) to a string representing
	 * the path commands of type 'C ...'
	 *
	 * @returns {string}
	 */
	svgPath() {
		return this.data.reduce((acc, point, i, a) => {
			if (i === 0) {
				return `M ${point[0]} ${point[1]}`
			} else {
				return `${acc} ${this.bezierCommand(point, i, a)}`
			}
		}, '')
	}

	/**
	 * Create the bezier curve command
	 *
	 * @param {Array.<number>} point - [x,y], current point coordinates
	 * @param {number} i - index of 'point' in the array 'a'
	 * @param {Array.<Array<number>>} a - complete array of points coordinates
	 * @returns {string} 'C x2,y2 x1,y1 x,y' SVG cubic bezier C command
	 */
	bezierCommand(point, i, a) {
		// start control point
		const [cpsX, cpsY] = this.controlPoint(a[i - 1], a[i - 2], point)
		// end control point
		const [cpeX, cpeY] = this.controlPoint(point, a[i - 1], a[i + 1], true)
		return `C ${cpsX},${cpsY} ${cpeX},${cpeY} ${point[0]},${point[1]}`
	}

	/**
	 * Position of a control point
	 *
	 * @param {Array.<number>} current - [x, y] current point coordinates
	 * @param {Array.<number>} previous - [x, y] previous point coordinates
	 * @param {Array.<number>} next - [x, y] next point coordinates
	 * @param {boolean=} reverse - sets the direction
	 * @returns {Array.<number>} [x,y] a tuple of coordinates
	 */
	controlPoint(current, previous, next, reverse) {
		// When 'current' is the first or last point of the array
		// 'previous' or 'next' don't exist.
		// Replace with 'current'
		const p = previous || current
		const n = next || current  // The smoothing ratio
		const smoothing = 0.2  // Properties of the opposed-line
		const o = this.line(p, n)  // If is end-control-point, add PI to the angle to go backward
		const angle = o.angle + (reverse ? Math.PI : 0)
		const length = o.length * smoothing  // The control point position is relative to the current point
		const x = current[0] + Math.cos(angle) * length
		const y = current[1] + Math.sin(angle) * length
		return [x, y]
	}

	/**
	 * Properties of a line
	 *
	 * @param {Array.<number>} pointA - [x,y] coordinates
	 * @param {Array.<number>} pointB - [x,y] coordinates
	 * @returns {{length: number, angle: number}}
	 */
	line(pointA, pointB) {
		const lengthX = pointB[0] - pointA[0]
		const lengthY = pointB[1] - pointA[1]
		return {
			length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
			angle: Math.atan2(lengthY, lengthX),
		}
	}
}
