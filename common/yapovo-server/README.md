# Common/WebSocket Server

See the [common implementation](../yapovo-communication/README.md).

The `Server` class encapsulates all methods required to respect the Yapovo WebSocket Protocol.
