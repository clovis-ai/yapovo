/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"
import WebSocket from 'ws'

export default class Server {

	/**
	 * ID of control panel
	 * @type {number}
	 */
	static controlID = 1

	/**
	 * The WebSocket yapovo-server.
	 */
	#server

	/**
	 * The token provided by the control before the yapovo-server starts.
	 * @type int
	 */
	#token

	/**
	 * The connection to the control (or `undefined` if the control hasn't connected yet).
	 * @type WebSocket
	 */
	#control

	/**
	 * The connections to the different clients.
	 * @type {{WebSocket}}
	 */
	#clients = {}

	/**
	 * The callbacks corresponding to the action to perform on the yapovo-server when a message is received.
	 * @type {[Function]}
	 */
	#receptors = []

	/**
	 * Message types that are sent using unicast instead of broadcast.
	 * @type {[Message.Type|int]}
	 */
	#unicastTypes = [Message.Type.INTERNAL_WEBRTC_OFFER, Message.Type.INTERNAL_ICE_CANDIDATE]

	/**
	 * Last attributed id for Ws peers
	 * @type int
	 */
	#lastClientID = Server.controlID

	/**
	 * Starts a WebSocket yapovo-server.
	 * @param port {int|string} The port on which the yapovo-server should listen
	 * @param token {int} The token provided by the control
	 */
	constructor(port, token) {
		this.#server = new WebSocket.Server({
			port: port,
		})
		this.#token = token

		this.#server.on("listening", () => {
			const m = new Message(Message.Type.IPC, "server_connected")
			process.send(m.serialize())
		})

		this.#server.on("connection", connection => {
			connection.on("message", message => {
				const msg = Message.deserialize(message)

				if (msg.type === Message.Type.IDENTIFICATION) {
					this.#handleIdentification(msg, connection)
				} else {
					this.#handleOtherMessages(msg, connection)
				}
			})
		})
	}

	//region Handle messages & authentication

	/**
	 * Sets up an incoming request for authentication
	 * @param msg {Message} The authentication request
	 * @param connection {WebSocket} The connection from which the message originated
	 */
	#handleIdentification(msg, connection) {
		const payload = msg.payload

		if (payload.mode === "CONTROL") {
			if ("token" in payload)
				this.#verifyControl(payload, connection)
			else
				this.send(connection, new Message(Message.Type.WARNING, "yapovo-server error: missing 'token' field for IDENTIFICATION type"))
		} else if (payload.mode === "CLIENT") {
			this.#verifyClient(connection)
		} else {
			this.send(connection, new Message(Message.Type.WARNING, "yapovo-server error: invalid 'mode' for IDENTIFICATION type"))
		}
	}

	/**
	 * Verifies the token of the control
	 * @param payload The payload of the authentication request
	 * @param connection {WebSocket} The connection from which the message originated
	 */
	#verifyControl(payload, connection) {
		// noinspection JSUnresolvedVariable Already verified in #handleOtherMessages
		if (this.#control === undefined && payload.token === this.#token)
			this.#control = connection
		else
			this.send(connection, new Message(Message.Type.WARNING, "yapovo-server error: identification failed: wrong token or yapovo-server already connected to control"))
	}

	/**
	 * Verifies the authentication of a yapovo-client
	 * @param connection The connection from which the message originated
	 */
	#verifyClient(connection) {
		if (!(Object.values(this.#clients).includes(connection))) {
			this.#lastClientID++
			this.#clients[this.#lastClientID] = connection
			this.send(this.#control, new Message(Message.Type.NEW_CLIENT_CONNECTION, {
				clientID: this.#lastClientID,
			}))
		} else
			this.send(connection, new Message(Message.Type.WARNING, "yapovo-server warning: yapovo-client is already connected"))
	}

	/**
	 * Handle incoming messages (other than IDENTIFICATION)
	 * @param msg {Message} The received message
	 * @param connection {WebSocket} The connection the message originated from
	 */
	#handleOtherMessages(msg, connection) {
		const allowedClients = {[Server.controlID]: this.#control, ...this.#clients}
		if (Object.values(allowedClients).includes(connection)) {
			if (this.#unicastTypes.includes(msg.type)) {
				// Sends the message only to target
				this.send(allowedClients[msg.payload.target], msg)
			} else {
				// Broadcast to all clients, except the current one
				Object.values(allowedClients).forEach(client => {
					if (client !== connection)
						this.send(client, msg)
				})
			}

			this.#receptors[msg.type]?.(msg)
		} else {
			console.error("Message was not transmitted to other clients: emitter is not allowed")
			// do NOT reply to people who didn't identify themselves first (they don't respect the protocol === attackers)
		}
	}

	//endregion

	/**
	 *
	 * @param connection {WebSocket}
	 * @param message
	 */
	send(connection, message) {
		connection.send(message.serialize())
	}

	/**
	 *
	 * @param type {int}
	 * @param callback {Function}
	 */
	onReception(type, callback) {
		this.#receptors[type] = callback
	}
}
