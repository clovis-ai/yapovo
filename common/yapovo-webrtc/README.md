# Common/WebRTC

The module `webrtc` is a common interface for WebRTC peer to peer communication, aimed to help
development for Yapovo.

## Usage

The static method `RTCFromControl()` returns an instance of `WebRTC` class which is prepared to be
used in the control panel.

The static method `RTCFromClient()` returns an instance of `WebRTC` class which is prepared to be
used clients such as the output panel or the drawing panel.

These methods are used respectively in `authenticateControl` and `authenticateClient` so that the
WebRTC connection process is invisible for both clients and control panel.

### Control panel

To send a track via WebRTC, you just have to call the method `send` of your `Client` instance and
pass it the `Message` of the correct type (either `TRACK`, `REMOVE_TRACK` or `SVG`).

Example:

```js
this.client.send(new Message(Message.Type.TRACK, {
	id: id,
	track: track,
	stream: stream,
}))
```

When a client is disconnected, the webRTC connection is removed from the list of available
connections.

### Clients

Clients must give callbacks to the `Client` object thanks to the `onReception` method for WebRTC
message types.

Example:

```js
Socket.onReception(Message.Type.TRACK, receiveVideo)

Socket.onReception(Message.Type.SVG, receiveSVG)
```

## Workflow and Implementation

The process is easy:

We first establish a dataChannel connection (1):

* A client authenticates itself with the WS Server
* The WS Server tells the control panel that a new client is connected
* The control panel sends a `video-offer` and some ICE candidates to the client (targeted WS
  messages)
* The client processes the offer and sends a `video-answer`
* The client processes the ICE Candidates, and the channel is open.

Then, a handshake process is made:

* Control panel sends via the new WebRTC data channel a `data-channel-created` message.
* The client answers via the new WebRTC data channel by sending a `data-channel-connected`

Then, a new WebRTC connection is established between the control panel, and the client using
process (1).

The callback for received tracks is given to the new WebRTC Stream connection peer.

3 types of WS messages are used to establish the WebRTC connection:

* ```Message.Type.NEW_CLIENT_CONNECTION```
* ```Message.Type.INTERNAL_WEBRTC_OFFER```
* ```Message.Type.INTERNAL_ICE_CANDIDATE```
* ```Message.Type.INTERNAL_WEBRTC_HANDSHAKE```

Some message types now correspond to targeted messages.

Their payload begins with the following pattern:

```json
{
	"sender": 1,
	"target": 2
}
```