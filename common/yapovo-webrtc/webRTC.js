/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"

export default class WebRTC {

	/**
	 * ID of control panel (given by WS)
	 * @type {int}
	 */
	static controlID = 1

	/**
	 * RTCPeerConnection object
	 * @type {RTCPeerConnection}
	 */
	peer

	/**
	 * Unique id representing the sender
	 * @type {int}
	 */
	senderID

	/**
	 * Unique id representing the target
	 * @type {int}
	 */
	targetID

	/**
	 *  An array of RTCRtpSender (tracks id) which identify tracks that are transmitted to the peer
	 * @type {Array<RTCRtpSender>}
	 */
	RTCRtpSenderIDs = []

	/**
	 * Current data channel used to transfer serialized objects or blobs through a WebRTC Peer connection
	 */
	dataChannel

	/**
	 *
	 * @param senderID {int}
	 * @param targetID {int}
	 * @param socket {Client}
	 */
	constructor(senderID, targetID, socket) {
		console.log("WebRTC: New peer")
		this.socket = socket
		this.targetID = targetID
		this.senderID = senderID

	}

	/**
	 * Gets WebRTC instance for control panel
	 * @param targetID {int}
	 * @param socket {Client}
	 * @constructor
	 */
	static RTCFromControl(targetID, socket) {
		const webRTC = new WebRTC(WebRTC.controlID, targetID, socket)
		webRTC.createPeerConnection(targetID)

		socket.onReception(Message.Type.INTERNAL_WEBRTC_OFFER, (msg) => {
			webRTC.handleVideoAnswerMsg(msg.payload).then(() => {
				console.log("WebRTC: Peer ready")
			})
		}, targetID)

		return webRTC
	}

	/**
	 * Gets WebRTC instance for clients and adds callback for ICE Candidate message type
	 * @param offer {Object} Payload of offer message
	 * @param socket {Client}
	 * @param onTrack {function} Optional : function to call when a track is received
	 * @constructor
	 */
	static RTCFromClients(offer, socket, onTrack = null) {
		let webRTC = socket.WebRTCStreamFromControl
		if (!onTrack || (onTrack && !webRTC)) {
			webRTC = new WebRTC(offer.target, offer.sender, socket)
			onTrack ? socket.WebRTCStreamFromControl = webRTC : null
		}

		socket.onReception(Message.Type.INTERNAL_ICE_CANDIDATE, (msg) => {
			webRTC.handleNewICECandidateMsg(msg)
		})

		//Processes the video-offer and adding callback for track reception
		webRTC.handleVideoOfferMsg(offer).then(() => {
			onTrack ? webRTC.onReceptionTrack(onTrack) : undefined
		})

		return webRTC
	}

	/**
	 * Creates peer connection and sets the event handlers
	 * @param targetID {int} Optional : ID of the target used to set callback (for control panel)
	 */
	createPeerConnection(targetID = 0) {
		this.peer = new RTCPeerConnection({
			iceServers: [],
		})
		this.peer.onnegotiationneeded = this.handleNegotiationNeededEvent.bind(this)
		this.peer.onicecandidate = this.handleICECandidateEvent.bind(this)
		this.socket.onReception(Message.Type.INTERNAL_ICE_CANDIDATE, this.handleNewICECandidateMsg.bind(this), targetID)
	}

	/**
	 * Creates a data channel in WebRTC Peer
	 * @param label
	 * @return {RTCDataChannel | undefined}
	 */
	createDataChannel(label) {
		this.dataChannel = this.peer?.createDataChannel(label)
	}

	/**
	 *
	 * @param e {Event}
	 */
	reportError(e) {
		console.error("Error : " + e)
	}

	/**
	 * Sends the provided payload to the WS yapovo-server with specified type.
	 * @param payload {Object}
	 * @param type {Message.Type}
	 */
	send(type, payload) {
		this.socket.send(new Message(type, payload))
	}

	/**
	 * Creates and sends the offer to target when negotiationneeded event is triggered (as caller)
	 */
	handleNegotiationNeededEvent() {
		this.peer.createOffer().then(offer => {
			return this.peer.setLocalDescription(offer)
		})
			.then(() => {
				this.send(Message.Type.INTERNAL_WEBRTC_OFFER, {
					sender: this.senderID,
					target: this.targetID,
					type: "video-offer",
					sdp: this.peer.localDescription,
				})
			})
			.catch(this.reportError)
	}

	/**
	 * Process the answer offer from the other peer
	 * @param payload {Object}
	 */
	async handleVideoAnswerMsg(payload) {

		const desc = new RTCSessionDescription(payload.sdp)
		this.peer.setRemoteDescription(desc).then(() => {
			console.log("WebRTC: Peer configured")
		})
	}

	/**
	 * Receives and processes an offer from other peer (as callee).
	 * Sends the answer
	 * @param payload {Object}
	 */
	async handleVideoOfferMsg(payload) {
		if (!this.peer) {
			this.createPeerConnection()
		}

		const desc = new RTCSessionDescription(payload.sdp)

		this.peer.setRemoteDescription(desc).then(() => {
			return this.peer.createAnswer()
		})
			.then(answer => {
				return this.peer.setLocalDescription(answer)
			})
			.then(() => {
				this.send(Message.Type.INTERNAL_WEBRTC_OFFER, {
					sender: this.senderID,
					target: this.targetID,
					type: "video-answer",
					sdp: this.peer.localDescription,
				})
			})
			.catch(this.reportError)
	}

	/**
	 * Sends ICE candidate message to other peer when ICECandidateEvent is triggered by RTCPeer.
	 * @param event {RTCPeerConnectionIceEvent}
	 */
	handleICECandidateEvent(event) {
		if (event.candidate) {
			this.send(Message.Type.INTERNAL_ICE_CANDIDATE, {
				sender: this.senderID,
				target: this.targetID,
				type: "new-ice-candidate",
				candidate: event.candidate,
			})
		}
	}

	/**
	 * Adds ICE Candidate to RTC Peer when received
	 * @param msg {Object}
	 */
	handleNewICECandidateMsg(msg) {
		const candidate = new RTCIceCandidate(msg.payload.candidate)
		this.peer.addIceCandidate(candidate).catch(this.reportError)
	}

	/**
	 * Sets function to call when a track is received
	 * @param callback {function}
	 * @constructor
	 */
	onReceptionTrack(callback) {
		this.peer.ontrack = callback
	}

	/**
	 * Sets function to call when a connection closes
	 * @param callback {function}
	 * @constructor
	 */
	onConnectionClose(callback) {
		this.peer.onconnectionstatechange = () => {
			if (this.peer.connectionState !== "connecting" && this.peer.connectionState !== "connected") {
				callback()
			}
		}
	}

	/**
	 * Adds a track to RTCPeer
	 * @param track {MediaStreamTrack}
	 * @param stream {MediaStream}
	 */
	addTrack(track, stream) {
		this.RTCRtpSenderIDs.push(this.peer.addTrack(track, stream))
	}

	/**
	 * Replaces track which has trackID ID.
	 * @param trackID {int}
	 * @param newTrack {MediaStreamTrack}
	 */
	replaceTrack(trackID, newTrack) {
		this.RTCRtpSenderIDs[trackID]?.replaceTrack(newTrack).then(() => {
			console.log("WebRTC: Track replaced")
		})
	}

	/**
	 * Removes a track from RTCPeer
	 * @param trackID {int}
	 */
	removeTrack(trackID) {
		if (this.RTCRtpSenderIDs[trackID]) {
			this.peer.removeTrack(this.RTCRtpSenderIDs[trackID])
			this.RTCRtpSenderIDs.splice(trackID, 1)
		}
	}

	/**
	 * Sends a message to peer vua WebRTC Data channels
	 * @param message {Message}
	 */
	sendByDataChannel(message) {
		if (!this.dataChannel) {
			this.createDataChannel("data")
			this.dataChannel.onopen = () => this.dataChannel.send(message.serialize())
		} else {
			this.dataChannel.send(message.serialize())
		}
	}

	initializeDataChannel(onData) {
		this.peer.ondatachannel = event => {
			console.log("WebRTC: New data channel")
			this.dataChannel = event.channel
			this.setCallbackDataChannel(onData)
		}
	}

	setCallbackDataChannel(onData) {
		this.dataChannel.onmessage = event => {
			onData(event)
		}
	}
}


