/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import WindowSlot from "./WindowSlot"
import interact from "interactjs"
import Layout from "./Layout"

/**
 * Translates a window to specific coordinates.
 * @param slot {WindowSlot} An WindowSlot element
 * @param x {Number} The x coordinate
 * @param y {Number} The y coordinate
 * The function translate3d uses the GPU instead of the CPU in order to have a smooth transformation.
 */
export function translate(slot, x, y) {
	slot.x = x
	slot.y = y
	slot.div.style.transform = `translate3d(${x}px, ${y}px, 0)`
}

/**
 * Moves the selected windows according to the movement of the cursor.
 * @param event {InteractEvent} An InteractJS specific event
 */
export function dragMoveListener(event) {
	const divs = document.querySelectorAll("div.selected")
	divs.forEach(
		/**
		 * @param div {HTMLElement}
		 */
		(div) => {
			const slot = WindowSlot.get(div.id)
			translate(slot, slot.x + event.delta.x, slot.y + event.delta.y)
		})
}

/**
 * Minimises a window on window-picker entry
 * @param event {InteractEvent} An InteractJS specific event
 */
function dragEnterWindowPickerListener(event) {
	// Gets the dragged element
	const target = event.relatedTarget

	// Get the WindowSlot associated
	const slot = WindowSlot.get(target.id)

	// If the WindowSlot is not already minimized, minimize it and recenter the window on the cursor
	if (!slot.isMinimized() && isWindowPickerVisible()) {
		slot.minimize()
		center(event.dragEvent.client, slot, slot.img.offsetWidth, slot.img.offsetHeight)
	}
}

/**
 * Adds the window to the window-picker on drop and stop the screen sharing
 * @param event {InteractEvent} An InteractJS specific event
 */
function dropWindowPickerListener(event) {
	// Gets the dragged element
	const target = event.relatedTarget

	// Get the WindowSlot associated
	const slot = WindowSlot.get(target.id)

	// Make sure the WindowSlot is minimized on the drop event
	if (slot.isMinimized() && isWindowPickerVisible()) {
		// Change the position so that the element aligns
		target.style.position = "relative"

		// The video is now hidden, so stop the screenSharing
		slot.screenSharing.stop()

		// Add the element to the window picker
		document.querySelector("#window-picker").append(slot.div)

		// Resets the element position
		translate(slot, 0, 0)

		slot.removeFromLayout(Layout.getCurrentLayout())
	}
}

/**
 * Maximizes a window on layout entry
 * @param event {InteractEvent} An InteractJS specific event
 */
function dragEnterLayout(event) {
	// Gets the dragged element
	const target = event.relatedTarget

	// Get the WindowSlot associated
	const slot = WindowSlot.get(target.id)

	// If the WindowSlot is not already maximized, maximizes it, starts the screenSharing, centers the window on the cursor and adds the element to the layout container
	if (!slot.isMaximized()) {
		slot.maximize()
		slot.screenSharing.start().catch(console.error)
		center(event.dragEvent.client, slot, slot.img.width, slot.img.height)
		document.querySelector("#layout").append(slot.div)
	}
}

function dropEnterLayout(event) {
	const target = event.relatedTarget
	const slot = WindowSlot.get(target.id)
	const layout = Layout.getCurrentLayout()
	slot.saveToLayout(layout)
}

/**
 * Restricts the position of the dragged windows inside the screen
 * @param event {InteractEvent} An InteractJS specific event
 */
function restrictParent(event) {
	const divs = document.querySelectorAll("div.selected")
	divs.forEach(
		/**
		 * @param div {HTMLElement}
		 **/
		(div) => {
			const slot = WindowSlot.get(div.id)
			if (slot.x < 0)
				slot.x = 0
			else if (slot.x + slot.div.offsetWidth > window.innerWidth)
				slot.x = window.innerWidth - slot.div.offsetWidth
			if (slot.y < 0)
				slot.y = 0
			else if (slot.y + slot.div.offsetHeight > window.innerHeight + 4)
				slot.y = window.innerHeight - slot.div.offsetHeight + 4
			translate(slot, slot.x, slot.y)
		})
}

/**
 * Centers an element on the cursor position.
 * @param mouse {Point} The mouse position
 * @param slot {WindowSlot} An HTML element
 * @param width {Number} The element's width
 * @param height {Number} The element's height
 */
function center(mouse, slot, width, height) {
	const x = mouse.x - width / 2
	const y = mouse.y - height / 2
	slot.div.style.position = "absolute"
	translate(slot, x, y)
}

/**
 * Makes the elements of a CSS class draggable
 * @param className {String} CSS class name
 * @param move {function} The move listener
 */
export function makeDraggable(className, move) {
	interact(className)
		.draggable({
			autoScroll: false,
			listeners: {
				move: move,
				end: restrictParent,
			},
		})
}

/**
 * Creates a dropzone
 * @param className {String} CSS class name
 * @param accept {String} accepted CSS classes
 * @param onDragEnter {function} Listener for the entrance
 * @param onDrop {function} Listener for the drop
 */
function createDropzone(className, accept, onDragEnter = null, onDrop = null) {
	interact(className)
		.dropzone({
			// On drop accepts only this CSS classes
			accept: accept,
			// Considers the element to be inside of the dropzone according to the cursor position
			overlap: 'pointer',
			ondragenter: onDragEnter,
			ondrop: onDrop,
		})
}

/**
 * Unselects all the selected windows
 */
function unselectWindows() {
	const divs = document.querySelectorAll("div.selected")
	divs.forEach(
		/**
		 * @param div {HTMLElement}
		 */
		(div) => {
			div.classList.remove("selected")
		})
}

/**
 * Handles a simple selection of window
 */
export function simpleSelectWindow() {
	if (!this.classList.contains("selected")) {
		unselectWindows()
		this.classList.add("selected")
	}
}

/**
 * Handles a multiple selection of windows
 */
function multipleSelectWindow() {
	this.classList.toggle("selected")
}

/**
 * Handles multiple selection of windows using CTRL
 * @param keyEventType {string}
 * @param onRemovingListener {function}
 * @param onAddingListener {function}
 */
function handleMultipleSelection(keyEventType, onRemovingListener, onAddingListener) {
	document.addEventListener(keyEventType, (event) => {
		if (event.key === "Control") {
			const layoutBackground = document.querySelector("#layout-background")
			keyEventType === "keydown" ? layoutBackground.removeEventListener("click", unselectWindows) : layoutBackground.addEventListener("click", unselectWindows)
			const divs = document.querySelectorAll(".window-slot")
			divs.forEach(
				/**
				 * @param div {HTMLElement}
				 */
				(div) => {
					div.removeEventListener("mousedown", onRemovingListener)
					div.addEventListener("mousedown", onAddingListener)
				})
		}
	})
}

/**
 * Adds all the required listeners and makes elements interactive
 */
export function applyHTMLTransformations() {
	makeDraggable('.container-window-picker', dragMoveListener)

	createDropzone("#window-picker-toggle", '.window-slot, .container-window-picker', dragEnterWindowPickerListener, dropWindowPickerListener)
	createDropzone("#window-picker", '.window-slot, .container-window-picker', null, dropWindowPickerListener)
	createDropzone("#layout", '.window-slot, .container-window-picker', dragEnterLayout, dropEnterLayout)

	// Handles the disappearance of the window-picker
	document.querySelector("#window-picker").addEventListener("mouseleave", (event) => {
		event.target.style.visibility = "hidden"
	})

	// Handles the apparition of the window-picker
	document.querySelector("#window-picker-toggle").addEventListener("mouseenter", () => {
		document.querySelector("#window-picker").style.visibility = "visible"
	})

	// Handles the multiple selection of windows when CTRL is pressed
	handleMultipleSelection("keydown", simpleSelectWindow, multipleSelectWindow)

	// Handles the simple selection of window when CTRL is not pressed
	handleMultipleSelection("keyup", multipleSelectWindow, simpleSelectWindow)

	// Handles the initial simple selection of window (CTRL is up but the event is not triggered at launch)
	document.querySelector("#layout-background").addEventListener("click", unselectWindows)
}

/**
 * Checks if the window picker is visible
 * @return {boolean} True if the window picker is visible
 */
function isWindowPickerVisible() {
	return document.querySelector("#window-picker").style.visibility === "visible"
}
