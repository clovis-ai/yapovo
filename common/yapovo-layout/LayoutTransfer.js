/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"
import WindowSlot from "./WindowSlot"

export class LayoutTransfer {

	/**
	 * Client for Ws and WebRTC
	 * @type {Client}
	 */
	client

	/** A map of all windowSlots
	 * @type {Map<String, WindowSlot>}
	 */
	WindowSlots

	/**
	 * A map of all Layouts
	 * @type {Map<String, Layout>}
	 */
	Layouts

	/**
	 * A map of all tracks
	 * @type {Map<String, MediaStreamTrack>}
	 */
	Tracks

	/**
	 * Callback to call when a layout is received
	 * @type {function(Map<String, Layout>, Layout, Map<String, WindowSlot>):void}
	 */
	onLayoutReception

	/**
	 * Callback to call when a window slot is received
	 * @type {function(Map<String, WindowSlot>, [WindowSlot]):void}
	 */
	onWindowSlotReception

	/**
	 * Callback to call when a track is received
	 * @type {function(Map<String, WindowSlot>, MediaStreamTrack, MediaStream):void}
	 */
	onTrackReception

	/**
	 *
	 * @param client {Client}
	 */
	constructor(client) {
		this.client = client
	}

	/**
	 *
	 * @param onLayoutReception {function(Map<String, Layout>, Layout, Map<String, Layout>):void}
	 * @param onWindowSlotReception {function(Map<String, WindowSlot>, [WindowSlot]):void}
	 * @param onTrackReception {function(Map<String, MediaStreamTrack>, MediaStreamTrack):void}
	 */
	startReception(onLayoutReception, onWindowSlotReception, onTrackReception) {
		this.WindowSlots = new Map()
		this.Layouts = new Map()
		this.Tracks = new Map()
		this.onLayoutReception = onLayoutReception
		this.onWindowSlotReception = onWindowSlotReception
		this.onTrackReception = onTrackReception
		this.client.onReception(Message.Type.LAYOUT, this.receiveLayout.bind(this))
		this.client.onReception(Message.Type.WINDOW_SLOT, this.receiveWindowSlots.bind(this))
		this.client.onReception(Message.Type.TRACK, this.createHTMLVideo.bind(this))
	}

	/**
	 * Starts the emission of layouts, windowsSlots and Tracks
	 */
	startEmission() {
		this.client.onReception(Message.Type.WINDOW_SLOT, this.onReceptionWindowSlotRequest.bind(this))
		this.client.onReception(Message.Type.TRACK_REQUEST, this.onReceptionTrackRequest.bind(this))
	}

	//region Receive

	/**
	 * Callback for layout reception
	 * Received WindowSlotPosition that are not already known are stored for future usage.
	 * @param msg {Message}
	 */
	receiveLayout(msg) {
		const newSlots = new Map()
		const oldSlots = new Map()
		const layout = msg.payload
		layout.windowSlotPositions.forEach(WindowSlotPosition => {
			if (!this.WindowSlots.has(WindowSlotPosition.slotId)) {
				newSlots.set(WindowSlotPosition.slotId, WindowSlotPosition)
			} else {
				oldSlots.set(WindowSlotPosition.slotId, this.WindowSlots.get(WindowSlotPosition.slotId))
			}
		})
		this.Layouts.set(layout.layoutId, layout)
		this.onLayoutReception?.(this.Layouts, layout, oldSlots)
		this.askForWindowSlots(newSlots)
	}

	/**
	 *  Asks unknown WindowSlots to control panel
	 * @param newSlots {Map<string, WindowSlotPosition>}
	 */
	askForWindowSlots(newSlots) {
		if (newSlots.size > 0) {
			this.client.send(new Message(Message.Type.WINDOW_SLOT, {
				type: "request",
				slots: Array.from(newSlots.keys()),
			}))
		}
	}

	/**
	 * Callback for WindowSlot reception
	 * Received TrackIDs that are not already known are stored for future usage.
	 * @param msg {Message}
	 */
	receiveWindowSlots(msg) {
		const newTracks = []
		const {payload} = msg
		payload.slots.forEach(slot => {
			this.WindowSlots.set(slot.slotId, slot)
			if (!this.Tracks.has(slot.trackId)) {
				newTracks.push(slot.trackId)
			}
		})
		this.onWindowSlotReception?.(this.WindowSlots, payload.slots)
		this.askForTracks(newTracks)
	}

	/**
	 * Asks Control Panel for unknown tracks
	 * @param newTracks {[String]}
	 */
	askForTracks(newTracks) {
		if (newTracks.length > 0) {
			this.client.send(new Message(Message.Type.TRACK_REQUEST, {
				tracks: newTracks,
			}))
		}
	}

	/**
	 * Fill HTML video element and creates new ones if necessary
	 * This method can be modified in the future to adapt with drawing panel (some of the logic could be moved to callback function (onTrackReception)
	 * @param event {RTCTrackEvent}
	 */
	createHTMLVideo(event) {
		const stream = event.streams[0]
		const track = event.track
		this.Tracks.set(track.id, track)
		this.onTrackReception?.(this.WindowSlots, track, stream)
	}

	//endregion

	//region Send

	/**
	 * Callback for WindowSlot request
	 * @param msg {Message}
	 */
	onReceptionWindowSlotRequest(msg) {
		const slotsToSend = []
		if (msg.payload.type === "request") {
			const slots = msg.payload.slots
			slots.forEach(slotId => {
				const slot = WindowSlot.get(slotId)
				slotsToSend.push({
					slotId: slotId,
					title: slot.title.innerHTML,
					trackId: slot.screenSharing.trackId,
				})
			})
			this.client.send(new Message(Message.Type.WINDOW_SLOT, {
				type: "answer",
				slots: slotsToSend,
			}))
		}
	}

	/**
	 * Callback for Track requests
	 * @param msg {Message}
	 */
	onReceptionTrackRequest(msg) {
		const {payload} = msg
		payload.tracks.forEach(trackID => {
			const {track, stream} = WindowSlot.getTrack(trackID)
			this.client.send(new Message(Message.Type.TRACK,
				{
					"id": -1,
					"track": track,
					"stream": stream,
				}))
		})
	}

	//endregion

}
