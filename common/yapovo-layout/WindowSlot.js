/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import ScreenSharing from "yapovo-electron/ScreenSharing"
import interact from "interactjs"
import {dragMoveListener, makeDraggable, simpleSelectWindow, translate} from "./DragDrop"
import {WindowSlotPosition} from "./WindowSlotPosition"
import Layout from "./Layout"

/**
 * WindowSlot
 *
 * @class WindowSlot
 * @classdesc A WindowSlot correspond to a captured
 * window and his particular annotation. A particular
 * WindowSlot can be identified by its id.
 */

export default class WindowSlot {

	/**
	 * A map of WindowSlot, indexed by WindowSlot id.
	 * @type {Map<string, WindowSlot>}
	 * @private
	 */
	static #windowSlots = new Map()

	/**
	 * A map of Tracks, indexed by TrackId.
	 * @type {Map<String, MediaStreamTrack>}
	 */
	static #tracks = new Map()

	/**
	 * Get a WindowSlot in the windowSlots Map
	 * @param id {String} The WindowSlot key in the Map
	 * @returns {WindowSlot} The WindowSlot
	 */
	static get = this.#windowSlots.get.bind(this.#windowSlots)

	/**
	 * Determine if WindowSlot corresponding to an id is in the windowSlots Map
	 * @param id {String} The WindowSlot key
	 * @returns {Boolean} Is inside the Map or not
	 */
	static has = this.#windowSlots.has.bind(this.#windowSlots)

	/**
	 * Gets Track whose trackId is id
	 * @param id {String} TrackId
	 * @returns {MediaStreamTrack} The Track
	 */
	static getTrack = this.#tracks.get.bind(this.#tracks)

	/**
	 * Sets Track whose trackId is id
	 * @param id {String} TrackId
	 */
	static setTrack = this.#tracks.set.bind(this.#tracks)

	/**
	 * The WindowSlot id
	 * @type String
	 * @public
	 */
	id

	/**
	 * The WindowSlot abscissas
	 * This is needed to avoid concurrent position changes
	 * @type Number
	 * @public
	 */
	x

	/**
	 * The WindowSlot ordinates
	 * This is needed to avoid concurrent position changes
	 * @type Number
	 * @public
	 */
	y

	/**
	 * The WindowSlot div element which contains all the other elements
	 * @type HTMLElement
	 * @public
	 */
	div

	/**
	 * The ScreenSharing associated to the WindowSlot
	 * @type ScreenSharing
	 * @public
	 */
	screenSharing

	/**
	 * The WindowSlot HTML video element
	 * @type HTMLVideoElement
	 * @public
	 */
	video

	/**
	 * The title of the window which is shared
	 * @type {HTMLHeadingElement}
	 */
	title

	/**
	 * Construct a WindowSlot
	 * @param source {Electron.DesktopCapturerSource}
	 * @param client {Client}
	 */
	constructor(source, client) {
		this.id = `windowSlot-${source.id}`

		this.x = 0
		this.y = 0

		this.div = WindowSlot.#createContainer(source)
		document.querySelector("#window-picker").appendChild(this.div)

		this.video = WindowSlot.#createVideo()
		this.div.appendChild(this.video)

		const fps = 30
		this.screenSharing = new ScreenSharing(this.id, source.id, this.video, client, {
			width: 1920,
			height: 1080,
		}, fps)

		// Creates the window/screen thumbnail
		this.img = WindowSlot.#createThumbnail(source)
		this.div.append(this.img)

		// Displays the window/screen name
		this.title = WindowSlot.#createTitle(source)
		this.div.append(this.title)

		this.svg = WindowSlot.#createSVG()
		this.div.appendChild(this.svg)

		// Adds this WindowSlot to the global map.
		WindowSlot.#windowSlots.set(this.id, this)

		this.#makeInteractive()
	}

	/**
	 * Create a window picker container
	 * @param source {Electron.DesktopCapturerSource} The source associated
	 * @returns {HTMLDivElement} The created html element
	 */
	static #createContainer(source) {
		const elem = document.createElement("div")
		elem.className = "container-window-picker"
		elem.id = `windowSlot-${source.id}`
		return elem
	}

	/**
	 * Create a video element
	 * @returns {HTMLVideoElement} The created html element
	 */
	static #createVideo() {
		const video = document.createElement("video")
		video.style.display = "none"
		return video
	}

	/**
	 * Create a window picker thumbnail
	 * @param source {Electron.DesktopCapturerSource} The source associated
	 * @returns {HTMLImageElement} The created html element
	 */
	static #createThumbnail(source) {
		const img = document.createElement("img")
		img.id = `thumbnail-window-picker-${source.id}`
		img.className = "thumbnail-window-picker"
		img.alt = source.name
		img.src = source.thumbnail.toDataURL()
		return img
	}

	/**
	 * Create a window picker title
	 * @param source {Electron.DesktopCapturerSource} The source associated
	 * @returns {HTMLHeadingElement} The created html element
	 */
	static #createTitle(source) {
		const title = document.createElement("h3")
		title.id = `title-window-picker-${source.id}`
		title.className = "title-window-picker"
		title.textContent = source.name
		return title
	}

	/**
	 * Create a svg
	 * @returns {HTMLElement} The created html element
	 */
	static #createSVG() {
		const svg = document.createElement('svg')
		svg.style.display = "none"
		return svg
	}

	/**
	 * Resizes a window according to the movement of the cursor.
	 * @param event {InteractEvent} An InteractJS specific event
	 */
	static #resizableListener(event) {
		const slot = WindowSlot.#windowSlots.get(event.target.id)

		slot.video.style.width = `${(event.rect.width)}px`
		slot.video.style.height = `${(event.rect.height)}px`
		translate(slot, slot.x + event.deltaRect.left, slot.y + event.deltaRect.top)
	}

	/**
	 * Delete a WindowSlot from the windowSlots Map
	 */
	delete = () => WindowSlot.#windowSlots.delete(this.id)

	/**
	 * Make the WindowSlot draggable and resizable
	 */
	#makeInteractive() {
		// Handles the initial simple selection of window
		this.div.addEventListener("mousedown", simpleSelectWindow)

		const classe = `#${CSS.escape(this.div.id)}`
		// noinspection JSCheckFunctionSignatures
		interact(classe).resizable({
			// Resizes from all edges and corners
			edges: {
				left: true,
				right: true,
				bottom: true,
				top: true,
			},
			listeners: {
				move: WindowSlot.#resizableListener,
				end: () => this.saveToLayout(Layout.getCurrentLayout()),
			},
			modifiers: [
				// Minimum size
				interact.modifiers.restrictSize({
					min: {
						width: 50,
						height: 50,
					},
				}),
			],
		})

		makeDraggable(classe, dragMoveListener)
	}

	/**
	 * Adds or updates this WindowSlot in the specified Layout.
	 * @param layout {Layout}
	 */
	saveToLayout(layout) {
		if (this.isMaximized()) {
			const slotPosition = new WindowSlotPosition(
				this.id,
				this.x / window.innerWidth,
				this.y / window.innerHeight,
				this.div.offsetWidth / window.innerWidth,
				this.div.offsetHeight / window.innerHeight,
			)

			layout.addSlotOrUpdate(slotPosition)
			layout.send()
		}
	}

	/**
	 * Removes this WindowSlot from the specified layout
	 * @param layout {Layout}
	 */
	removeFromLayout(layout) {
		layout.removeSlot(this.id)
		layout.send()
	}

	/**
	 * Minimizes the size of the WindowSlot and show an image and the source name
	 */
	minimize() {
		this.video.style.display = "none"
		this.img.style.display = "inline-block"
		this.title.style.display = "inline-block"
		// Replace the element class
		this.div.classList.remove("window-slot")
		this.div.classList.add("container-window-picker")
	}

	/**
	 * Maximizes the size of the WindowSlot and show the video
	 */
	maximize() {
		this.video.style.display = "inline-block"
		this.img.style.display = "none"
		this.title.style.display = "none"
		// Replace the element class
		this.div.classList.remove("container-window-picker")
		this.div.classList.add("window-slot")
	}

	/**
	 * @returns {boolean} True if the WindowSlot is minimized
	 */
	isMinimized = () => this.div.classList.contains("container-window-picker")

	/**
	 * @returns {boolean} True if the WindowSlot is maximized
	 */
	isMaximized = () => this.div.classList.contains("window-slot")
}
