/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {stringify} from "svgson"

/**
 * Registers the observer for SVG
 * @param svg
 * @param video
 */
export function registerResizeObserver(svg, video) {
	let resizeObserver = new ResizeObserver(entries => {
		for (let entry of entries) {
			if (entry.contentBoxSize) {
				resizeSVG(svg, video)
			}
		}
	})

	resizeObserver.observe(video)
}

/**
 * Resizes the SVG
 * @param svg
 * @param video
 * @param slot {ClientWindowSlot | Object}
 */
export function resizeSVG(svg, video, slot = null) {
	svg.style.width = `${video.offsetWidth}px`
	svg.style.height = `${video.offsetHeight}px`
	if (slot && !slot.isViewboxSet) {
		const viewbox = `0 0 ${video.offsetWidth} ${video.offsetHeight}`
		svg.setAttribute("viewBox", viewbox)
		slot.isViewboxSet = true
	}
}

/**
 * Waits until the video is ready to play to resize
 * the SVG and to start the video.
 * @param svg
 * @param video
 */
export function registerVideoPlayListener(svg, video) {
	video.addEventListener("canplay", () => {
		video.play().catch(e => {
			console.log(e)
		}).then(() => {
			setTimeout(() => {
				resizeSVG(svg, video)
			})
		})
	})
}

/**
 * Converts JSON-formatted SVG to a plain SvgElement
 * @param json
 * @return {ChildNode}
 * @constructor
 */
export function JSONtoSVG(json) {
	const parser = new DOMParser()
	return parser.parseFromString(stringify(json), "text/html").body.firstChild.firstChild
}

/**
 * Creates an individual slot div, which contains a video and a SVG
 * @param stream
 * @return {{individualDiv: HTMLDivElement, svg: SVGSVGElement, video: HTMLVideoElement}}
 */
export function createIndividualSlotDiv(stream) {
	const individualDiv = document.createElement("div")
	individualDiv.classList.add("individualSlotDiv")

	const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
	individualDiv.appendChild(svg)

	const video = document.createElement("video")
	video.setAttribute("autoplay", "")
	video.setAttribute("muted", "")
	video.srcObject = stream
	individualDiv.appendChild(video)
	return {individualDiv, svg, video}
}
