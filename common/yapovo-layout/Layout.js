/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import WindowSlot from "./WindowSlot"
import {translate} from "./DragDrop"
import {WindowSlotPosition} from "./WindowSlotPosition"
import Message from "yapovo-communication"

/**
 * Layout
 *
 * @class Layout
 * @classdesc A Layout is a list of WindowSlot ids and the corresponding positions / sizes
 */
export default class Layout {

	/**
	 * A map of Layout, indexed by the Layout id.
	 * @type {Map<int, Layout>}
	 * @private
	 */
	static #layouts = new Map()

	/**
	 * The current layout id
	 * @type {int}
	 * @private
	 */
	static #currentLayout = 0

	/**
	 * The Layout id
	 * @type {int}
	 * @public
	 */
	id

	/**
	 * A list of a WindowSlot id, its x coordinate, its y coordinate, its width and its height
	 * @type {Map<String, WindowSlotPosition>}
	 * @private
	 */
	#slotsPosition = new Map()

	/**
	 * Client which is used to send messages
	 * @type {Client}
	 */
	client

	/**
	 *
	 * @param client {Client} WS/WebRTC Client
	 */
	constructor(client) {
		this.client = client
		this.id = Layout.#generateId()
		this.#createTabButton()
	}

	/**
	 * Generates the next ID.
	 * @return {int}
	 */
	static #generateId() {
		let id = 0
		for (const key of Layout.#layouts.keys())
			id = key + 1
		return id
	}

	/**
	 * Get the number of layouts
	 * @returns {int} The Map size
	 */
	static size = () => this.#layouts.size

	/**
	 * Get the current layout id
	 * @returns {int} The layout id
	 */
	static current = () => this.#currentLayout

	/**
	 * Get the current layout.
	 * @return {Layout}
	 */
	static getCurrentLayout = () => this.#layouts.get(this.#currentLayout)

	/**
	 * Load a WindowSlot in a specific Layout
	 * @param slotPosition {WindowSlotPosition} A List of a slot id, its x, its y, its width, and its height
	 */
	static #loadWindowSlot(slotPosition) {
		const slot = WindowSlot.get(slotPosition.slotId)
		slot.maximize()
		slot.div.style.position = "absolute"

		// Updates the element style
		slot.video.style.width = `${(slotPosition.width)}px`
		slot.video.style.height = `${(slotPosition.height)}px`

		// Translates the element
		translate(slot, slotPosition.x, slotPosition.y)

		// Start the ScreenSharing
		slot.screenSharing.start().catch(console.error).then(() => {
			console.log(slot)
		})

		document.querySelector('#layout').append(slot.div)
	}

	/**
	 * Minimize all the WindowSlot elements
	 */
	static #minimizeAllWindowSlots() {
		document.querySelectorAll(".window-slot").forEach(
			/**
			 * @param element {HTMLElement}
			 */
			element => {
				const slot = WindowSlot.get(element.id)
				slot.minimize()
				translate(slot, 0, 0)
				element.style.position = "relative"
				document.querySelector("#window-picker").append(element)
			},
		)
	}

	/**
	 * Loads a specific Layout.
	 * @param id {int} A Layout id
	 */
	static #load(id) {

		const newLayout = Layout.#layouts.get(id)
		newLayout.send()

		document.querySelector(`#layout${Layout.#currentLayout}`).classList.remove("selected")
		document.querySelector(`#layout${id}`).classList.add("selected")

		Layout.#minimizeAllWindowSlots()

		newLayout.#slotsPosition.forEach(Layout.#loadWindowSlot)

		// Stop the useless ScreenSharing
		document.querySelectorAll(".container-window-picker")
			.forEach(element => {
				WindowSlot.get(element.id).screenSharing.stop()
			})

		Layout.#currentLayout = id
	}

	/**
	 * Removes the current Layout.
	 */
	static remove() {
		const id = this.#currentLayout
		Layout.#layouts.delete(id)
		Layout.#load(Layout.#layouts.values().next().value.id)
		document.querySelector(`#layout${id}`).remove()
	}

	/**
	 * Stores the layout in the layouts map
	 */
	store() {
		Layout.#layouts.set(this.id, this)
	}

	/**
	 * Adds a WindowSlotPosition to Layout or updates it if it already exists.
	 * @param WindowSlotPosition {WindowSlotPosition}
	 */
	addSlotOrUpdate(WindowSlotPosition) {
		this.#slotsPosition.set(WindowSlotPosition.slotId, WindowSlotPosition)
	}

	/**
	 * Removes a WindowSlotPosition from Layout
	 * @param id {string}
	 */
	removeSlot(id) {
		this.#slotsPosition.delete(id)
	}

	/**
	 * Creates an HTML button and its listener
	 */
	#createTabButton() {
		// Creates a new layout button
		const currentLayoutButton = document.createElement("button")
		currentLayoutButton.classList.add("layout-button")
		currentLayoutButton.id = `layout${this.id}`
		currentLayoutButton.innerHTML = `${this.id}`
		if (this.id === 0)
			currentLayoutButton.classList.add("selected")

		currentLayoutButton.onclick = () => {
			if (this.id !== Layout.#currentLayout) {
				Layout.#load(this.id)
			}
		}
		document.querySelector("#layout-container").append(currentLayoutButton)
	}

	send() {
		this.client.send(new Message(Message.Type.LAYOUT, {
			layoutId: this.id,
			windowSlotPositions: Array.from(this.#slotsPosition.values()),
		}))
	}
}
