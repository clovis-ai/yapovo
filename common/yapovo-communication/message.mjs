/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

/**
 * A message that can be sent from one yapovo-client to another.
 */
export default class Message {

	// Select a random integer to avoid collision between clients
	// Worst case scenario, an ID collision shouldn't be an issue (they should only be used for logging), as per the README
	static lastId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)

	//region Types

	/**
	 * The different types of messages.
	 * @enum {int} An integer used to find which value was referenced.
	 */
	static Type = {

		/**
		 * This type represents 'no' type.
		 * Use this for unit tests.
		 * Never send a message with this type.
		 */
		UNDEFINED: 0,

		/**
		 * When a yapovo-client connects, it sends an IDENTIFICATION message to tell the yapovo-server who it is.
		 *
		 * The payload should be:
		 * {
		 *     "mode": <TYPE>,
		 *     "token": <INT>,
		 * }
		 * where <TYPE> is the type of yapovo-client:
		 * - "mode": "CONTROL"  in that case, "token" is mandatory and should be the integer given by the Electron app.
		 * - "mode": "CLIENT"   in that case, "token" is not needed.
		 */
		IDENTIFICATION: 1,

		/**
		 * Represents a video shape.
		 */
		SHAPE: 2,

		/**
		 * Represents an error/warning message.
		 *
		 * The payload should be a simple string containing an explanation of the error.
		 */
		WARNING: 3,

		/**
		 * Represents an IPC message.
		 */
		IPC: 4,

		/**
		 * Represents an offer that RTCPeerConnection sends and receives to establish WebRTC connection
		 *
		 * The payload should be:
		 * {
		 *     "sender": <INT>,
		 *     "target": <INT>,
		 *     "type": <TYPE>,
		 *     "sdp": <STRING>,
		 * }
		 * where <TYPE> is the type of offer:
		 * - "type": "video-offer"
		 * - "type": "video-answer"
		 */
		INTERNAL_WEBRTC_OFFER: 5,

		/**
		 * Represents an ICE candidate message used during webRTC negotiation
		 *
		 * The payload should be:
		 * {
		 *     "sender": <INT>,
		 *     "target": <INT>,
		 *     "type": <TYPE>,
		 *     "candidate": <STRING>
		 * }
		 * where <TYPE> is the type of offer:
		 * - "type": "new-ice-candidate"
		 */
		INTERNAL_ICE_CANDIDATE: 6,

		/**
		 * Used to make a handshake between peers for WebRTC data channels
		 *
		 * The payload should be:
		 *
		 * {
		 *     "clientID": <INT>,
		 *     "type": <TYPE>,
		 * }
		 *
		 * where <TYPE> is the type of message:
		 * - "type": "data-channel-created"
		 * - "type": "data-channel-connected"
		 */
		INTERNAL_WEBRTC_HANDSHAKE: 7,

		/**
		 * Used to tell the control panel that a new connection has been established
		 *
		 * The payload should be:
		 * {
		 *     "clientID": <INT>,
		 * }
		 */
		NEW_CLIENT_CONNECTION: 8,

		/**
		 * Represents a MediaTrack message for control panel and clients
		 *
		 * The payload should be:
		 * {
		 *     "id": <INT>,
		 *     "track": <MediaTrack>,
		 *     "stream": <MediaStream>,
		 * }
		 */
		TRACK: 9,

		/**
		 * Represents a MediaTrack remove message for control panel and clients
		 *
		 * The payload should be:
		 * {
		 *     "id": <INT>,
		 * }
		 */
		REMOVE_TRACK: 10,

		/**
		 * Represents a SVG element
		 */
		SVG: 11,

		/**
		 * Represents a layout
		 *
		 * The payload should be:
		 * {
		 *     "layoutId": <INT>,
		 *     "windowSlotPositions": [<WindowSlotPosition>]
		 * }
		 * where <WindowSlotPosition> is :
		 * {
		 *     "slotId": <INT>,
		 *     "x": <FLOAT>,
		 *     "y": <FLOAT>,
		 *     "width": <FLOAT>,
		 *     "height": <FLOAT>,
		 * }
		 */
		LAYOUT: 12,

		/**
		 * Represents a window slot message
		 * It can be a request or an answer
		 *
		 * The payload should be:
		 *
		 * {
		 *     "type": <TYPE>,
		 *     "slots": [<WindowSlot> || <String>],
		 * }
		 *
		 * where <TYPE> is the type of layout message:
		 * - "type": "request"
		 * - "type": "answer"
		 */
		WINDOW_SLOT: 13,

		/**
		 * Represents a track request
		 *
		 * The payload should be:
		 *
		 * {
		 *     "tracks": [<String>],
		 * }
		 */
		TRACK_REQUEST: 14,

	}

	/**
	 * The type of the message
	 * @type {int}
	 */
	type

	/**
	 * The id of the message
	 * @type {int}
	 */
	id

	/**
	 * The payload of the message
	 * @type {Object}
	 */
	payload

	//endregion

	/**
	 * Constructs a Message.
	 * @param {int} type The type of this message.
	 * @param payload The contents of the message.
	 */
	constructor(type, payload) {
		// noinspection JSUnusedGlobalSymbols The ID is serialized with everything else, and is useful even if not directly accessed
		this.id = Message.lastId++

		this.type = type
		this.payload = payload
	}

	//region Serialization

	/**
	 * Reads a string representation of a Message.
	 * @param json {string} A string representation of this object.
	 * @returns {Message} A deserialized message.
	 * @see serialize
	 */
	static deserialize(json) {
		const msg = JSON.parse(json)
		return new Message(msg.type, msg.payload)
	}

	/**
	 * Writes this object to a string representation.
	 * @returns {string} A string representation of this object.
	 * @see deserialize
	 */
	serialize() {
		/*
		 * This implementation is minimal and unoptimized.
		 * The current goal is to have a prototype working as soon as possible,
		 * and to refactor these methods later when performance becomes a requirement.
		 */
		return JSON.stringify(this)
	}

	//endregion
}
