# Common/WebSocket

The module `websocket` is a common interface for client/server communication via websocket, aimed to
help development for Yapovo.

This module is the common implementation, a
specific [client implementation](../yapovo-client/README.md)
and a specific [server implementation](../yapovo-server/README.md) are available.

## Implementation and API

This module is structured around the [Message](message.mjs) class, which looks like:

```ts
class Message {
  id: int
  type: Message.Type
  payload: any
}
```

The ID is an integer that the different clients _try_ to keep unique, however no guarantees are
made. You can use it for logging to help differentiate messages, but do not use it for anything
which requires uniqueness.

The type of the message is used by the different implementations to allow multiple modules to talk
together via the same socket. The type should always be a value of the `Message.Type` array. See its
documentation for more details.

The payload represents the 'useful' data in the message. The expected payload is completely
different depending on the type of the message; see the type's documentation to know how the payload
should be structured.

## Connection sequence

Here are the different steps to connect a client to a server;

There are three entities:

- The `server`, which owns the WebSocket server (
  see [websocket-server](../yapovo-server/README.md)),
- The `control`, which is a WebSocket client that is authenticated to the `server` and has
  additional rights,
- The `clients`, which are normal WebSocket clients.

The `control` 'owns' the connection, and is responsible for initiating everything. The `control`
should provide the `server` a token (a randomized integer) in an external way to the `server`, using
a protocol that cannot be attacked by external actors (for example, via IPC). When the `server` is
up and running, all clients (both the `clients` and the `control`) must authenticate to it.

The `control` should authenticate by sending an `IDENTIFICATION` message with the following payload:

```json
{
  "mode": "CONTROL",
  "token": "the randomized number it sent to the yapovo-server here"
}
```

A `client` should authenticate by sending an `IDENTIFICATION` message with the following payload:

```json
{
  "mode": "CLIENT"
}
```

The `server` will completely ignore anyone that tries to connect without authenticating this way.
