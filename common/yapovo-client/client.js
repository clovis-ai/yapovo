/*
 *    Copyright 2021 Ivan Canet, Antoine Chartron, Rémi Dehenne,
 *    Sohaib Errabii, Maxime Girardet, Aurélien Moinel, Sébastien De Nerée
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import Message from "yapovo-communication"
import WebRTC from "yapovo-webrtc"

export default class Client {

	/**
	 * The connection to the yapovo-server.
	 * @type WebSocket
	 * @private
	 */
	socket

	/**
	 * An array of callbacks, indexed by the message type they correspond to.
	 * For some types (see #internalMessageTypes), callbacks are different depending on WebRTC target and an array of functions is used.
	 * @type {[Function] | [[Function]]}
	 * @private
	 */
	receptors = []

	/**
	 * An array of WebRTC Stream peers
	 * @type {[WebRTC]}
	 * @private
	 */
	WebRTCStreamPeers = []

	/**
	 * An array of WebRTC Data peers
	 * @type {[WebRTC]}
	 * @private
	 */
	WebRTCDataPeers = []

	/**
	 * A map of callbacks for WebRTC transmissions, indexed by the message type they correspond to.
	 * @type {Map}
	 * @private
	 */
	WebRTCSenders = new Map([
		[Message.Type.TRACK, this.sendTrack.bind(this)],
		[Message.Type.REMOVE_TRACK, this.sendRemoveTrack.bind(this)],
		[Message.Type.SVG, this.sendViaWebRTCDataChannel.bind(this)],
		[Message.Type.LAYOUT, this.sendViaWebRTCDataChannel.bind(this)],
	])

	/**
	 * Internal message types, which are used to establish WebRTC peer connections.
	 * This array is used to prepare callbacks for these types on the control panel.
	 * @type {[Message.Type|int]}
	 * @private
	 */
	internalMessageTypes = [
		Message.Type.INTERNAL_WEBRTC_OFFER,
		Message.Type.INTERNAL_ICE_CANDIDATE,
	]

	/**
	 *  An array of [track, stream] which are transmitted to the peer
	 * @type {[[MediaStreamTrack, MediaStream]]}
	 * @private
	 */
	medias = []

	/**
	 * A WebRTC data channel peer created by control panel (for clients)
	 * @type {WebRTC}
	 * @private
	 */
	WebRTCDataFromControl

	/**
	 * A WebRTC stream channel peer created by control panel (for clients)
	 * @type {WebRTC}
	 */
	WebRTCStreamFromControl

	/**
	 * Connects to a WebSocket yapovo-server.
	 * @param host {string} The hostname or IP address of the yapovo-server
	 * @param port {string|int} The port number on which the yapovo-server is running
	 */
	constructor(host, port) {
		this.socket = new WebSocket(`ws://${host}:${port}`)

		this.socket.onmessage = event => {
			const {type, payload} = Message.deserialize(event.data)
			const receptor = Array.isArray(this.receptors[type]) ? this.receptors[type][payload.sender] : this.receptors[type]
			receptor && receptor({type, payload})
		}

		this.close = this.socket.close
	}

	//region Authentication

	/**
	 * Is this socket alive?
	 * @returns {boolean} `true` if it is possible to send a message to this socket currently, `false` otherwise.
	 */
	get isAlive() {
		return this.socket.readyState === WebSocket.OPEN
	}

	/**
	 * Initializes properties for control panel and makes the authentication of the control panel
	 * @param token {int}
	 */
	authenticateControl(token) {
		this.internalMessageTypes.forEach(type => {
			this.receptors[type] = []
		})

		this.onReception(Message.Type.NEW_CLIENT_CONNECTION, msg => {
			this.addWebRTCData(msg)
		})

		const payload = {
			mode: "CONTROL",
			token: token,
		}
		this.send(new Message(Message.Type.IDENTIFICATION, payload))
		console.log("WS : Connected and authenticated!")
	}

	/**
	 * Adds a WebRTC Data connection (for control panel)
	 * @param message {Message}
	 */
	addWebRTCData(message) {

		const webrtcData = WebRTC.RTCFromControl(message.payload.clientID, this)
		this.WebRTCDataPeers.push(webrtcData)

		webrtcData.sendByDataChannel(new Message(Message.Type.INTERNAL_WEBRTC_HANDSHAKE, {
			clientID: message.payload.clientID,
			type: "data-channel-created",
		}))
		webrtcData.setCallbackDataChannel(this.addWebRTCStream.bind(this))
	}

	/**
	 * Adds a WebRTC Stream connection (for control panel)
	 * @param event {MessageEvent}
	 */
	addWebRTCStream(event) {
		const {type, payload} = Message.deserialize(event.data)
		if (type === Message.Type.INTERNAL_WEBRTC_HANDSHAKE && payload.type === "data-channel-connected") {
			const webrtcStream = WebRTC.RTCFromControl(payload.clientID, this)

			webrtcStream.onConnectionClose(() => {
				this.removeWebRTC(webrtcStream)
			})

			//Sending potentially already added tracks
			this.medias.forEach(([track, stream]) => {
				webrtcStream.addTrack(track, stream)
			})

			this.WebRTCDataPeers.forEach(webrtcData => {
				this.chooseCallbackDataChannel(webrtcData)
			})

			this.WebRTCStreamPeers.push(webrtcStream)
		}
	}

	/**
	 * Chooses the correct callback for WebRTC data channel
	 * @param webrtcData {WebRTC}
	 */
	chooseCallbackDataChannel(webrtcData) {
		webrtcData.setCallbackDataChannel(event => {
			const {type, payload} = Message.deserialize(event.data)
			this.receptors[type]?.({type, payload})
		})
	}


	/**
	 * Removes a WebRTC connection
	 * @param webrtc {WebRTC}
	 */
	removeWebRTC(webrtc) {
		const index = this.WebRTCStreamPeers.indexOf(webrtc)
		if (index > -1) {
			console.log("WebRTC: One peer is disconnected")
			this.WebRTCStreamPeers.splice(index, 1)
		}
	}

	/**
	 * Tells if yapovo-client has WebRTC connections
	 * @return {boolean}
	 */
	hasWebRTC() {
		return this.WebRTCStreamPeers.length > 0
	}

	//endregion

	prepareWebRTCReception(callback) {
		this.onReception(Message.Type.INTERNAL_WEBRTC_OFFER, (msg) => {
			if (msg.payload.type === "video-offer") {
				callback(msg)
			} else {
				this.send(new Message(Message.Type.WARNING, "Malformed WEBRTC_OFFER"))
			}
		})
	}

	/**
	 * Triggers WebRTC Data when a video-offer is received (for clients)
	 */
	prepareWebRTCDataReception() {

		this.prepareWebRTCReception(msg => {
			this.WebRTCDataFromControl = WebRTC.RTCFromClients(msg.payload, this, null)
			this.WebRTCDataFromControl.initializeDataChannel(this.makeHandshakeDataStream.bind(this))
		})
	}

	/**
	 * Processes WebRTC data handshake and sets new callback for data channel
	 * @param event {MessageEvent}
	 */
	makeHandshakeDataStream(event) {
		const {type, payload} = Message.deserialize(event.data)

		if (type === Message.Type.INTERNAL_WEBRTC_HANDSHAKE && payload.type === "data-channel-created") {
			this.chooseCallbackDataChannel(this.WebRTCDataFromControl)
			this.prepareWebRTCStreamReception()

			this.WebRTCDataFromControl.sendByDataChannel(new Message(Message.Type.INTERNAL_WEBRTC_HANDSHAKE, {
				clientID: payload.clientID,
				type: "data-channel-connected",
			}))

		} else {
			console.error("Incorrect data channel creation message")
		}
	}

	/**
	 * Triggers WebRTC Stream when a video-offer is received (for clients)
	 */
	prepareWebRTCStreamReception() {

		this.prepareWebRTCReception(msg => {
			WebRTC.RTCFromClients(msg.payload, this, this.receptors[Message.Type.TRACK])
		})
	}

	/**
	 * Makes the authentication of the clients
	 */
	authenticateClient() {
		const payload = {
			mode: "CLIENT",
		}
		if (this.receptors[Message.Type.TRACK]) {
			this.prepareWebRTCDataReception()
			this.send(new Message(Message.Type.IDENTIFICATION, payload))
			console.log("WS: Connected and authenticated!")
		} else {
			console.error("Please register a callback for Track message type before authenticating yapovo-client")
		}

	}

	/**
	 * Sends track message into WebRTC Peers
	 * id === -1 -> We add a new track
	 * Other value -> We replace the track
	 * @param message {Message}
	 */
	sendTrack({payload: {id, stream, track}}) {
		this.WebRTCStreamPeers.forEach(peer => {
			if (id === -1) {
				try {
					peer.addTrack(track, stream)
					this.medias.push([track, stream])
				} catch (e) {
					console.warn(e)
				}
			} else {
				peer.replaceTrack(id, track)
				this.medias[id] = [track, stream]
			}
		})
	}

	/**
	 * Sends remove track message into WebRTC Peers
	 * @param type
	 * @param id
	 */
	sendRemoveTrack({payload: {id}}) {
		this.WebRTCStreamPeers.forEach(peer => {
			peer.removeTrack(id)
		})
		this.medias.splice(id, 1)
	}

	sendViaWebRTCDataChannel(message) {
		this.WebRTCDataPeers.forEach(peer => {
			peer.sendByDataChannel(message)
		})
		this.WebRTCDataFromControl?.sendByDataChannel(message)
	}

	/**
	 * Sends a message via this Socket.
	 * @param message {Message} The message to be sent.
	 */
	send(message) {
		if (this.WebRTCSenders.has(message.type)) {
			this.WebRTCSenders.get(message.type)(message)
		} else {
			this.socket.send(message.serialize())
		}
	}

	/**
	 * Registers a callback on a certain type of messages.
	 * @param type {int} The type of message we want to get.
	 * @param callback {function} A callback which accepts a Message instance,
	 * that will be called whenever a message of that type is received.
	 * @param sender {int} Optional : Set callback for specific sender. When using this parameter, please add message type to #internalMessageTypes.
	 */
	onReception(type, callback, sender = 0) {
		if (sender === 0) {
			this.receptors[type] = callback
		} else {
			this.receptors[type][sender] = callback
		}
	}

	/**
	 * Adds a callback to the 'onClose' event from the socket.
	 * @param callback {function} The code to execute when the socket closes.
	 * @see WebSocket.onclose.
	 */
	onClose(callback) {
		this.socket.addEventListener('close', callback)
	}

	/**
	 * Adds a callback to the 'onOpen' event from the socket.
	 * @param callback {function} The code to execute when the socket opens.
	 * @see WebSocket.onopen.
	 */
	onOpen(callback) {
		this.socket.addEventListener('open', callback)
	}
}
