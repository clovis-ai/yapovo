# Common/WebSocket Client

See the [common implementation](../yapovo-communication/README.md).

The `Client` class encapsulates all methods required to respect the Yapovo WebSocket Protocol.
