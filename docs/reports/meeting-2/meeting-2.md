---
lang: fr
subject: "Projet au Fil de l'Année"
title: "YAPOVO : Compte-rendu de réunion n°2"
date: "18 décembre 2020"
author:
  - "Ivan Canet"
  - "Antoine Chartron"
  - "Rémi Dehenne"
  - "Sébastien de Nerée"
  - "Sohaib Errabii"
  - "Maxime Girardet"
  - "Aurélien Moinel"
keywords: [PFA, Projet au Fil de l'Année, S7, ENSEIRB-MATMECA, YAPOVO, Overlay, Compte-Rendu 2]

output:
    pdf_document

urlcolor: RoyalBlue
listings-disable-line-numbers: true
footer-left : $~$

header-includes: |
  \newcommand{\thesubject}{Projet au Fil de l'Année}
  \newcommand{\thetutor}{Floréal Morandat}
  \renewcommand{\lstlistingname}{Code}
  \renewcommand{\lstlistlistingname}{Liste des \lstlistingname s}

numbersections: false

---

```{=latex}
  \begin{center}
    \Large\textbf{\thesubject{}\\\thetitle}\\[0.5\baselineskip]
    \large Client : \thetutor\\[0.5\baselineskip]
    \large Équipe : \theauthor\\[0.5\baselineskip]
    \large{\makeatletter\@date\makeatother}
  \end{center}
```

Vendredi 18 décembre 2020, M. Morandat et l'équipe projet se sont réunis afin de valider la définition du besoin, présenter les Proof Of Concept (POC) développées et évoquer la suite du projet.


# Besoins et POC

Tout d'abord, la définition du besoin et les Proof Of Concept sont jugées satisfaisantes par M. Morandat.

Le POC de fusion de flux vidéo illustre un premier exemple de _compositing_ de fenêtres, dans lequel on superpose trois flux vidéo à des positions fixées.

Par la suite, la position des fenêtres devra être configurable via un fichier JSON : dans un premier temps, pour un nombre fixé de fenêtres, puis pour un nombre quelconque.

Enfin, la dernière étape consisterait à implémenter un éditeur graphique permettant de réorganiser et déplacer les fenêtres dans le flux vidéo de sortie, à la manière du logiciel libre OBS Studio.

# Suite du projet

M. Morandat a par la suite détaillé le contenu des prochains livrables et des prochaines étapes du développement du projet.

## Cahier des charges

L'équipe doit notamment rédiger un document de spécifications, afin de détailler précisément le besoin, la solution mise en place, ainsi que le planning prévisionnel.

Ce document sera rédigé de manière itérative, sous la forme d'un rapport. Il pourra et devra être soumis régulièrement au client pour validation et corrections ; cependant, la version finale, évaluée, devra être rendue au plus tard le 1er février 2021. Les versions intermédiaires n'auront pas d'impact direct sur la notation, mais elles ne pourront être soumises au client qu'au plus tard une semaine avant le rendu final. Chaque rendu sera en principe relu sous un à deux jours.

M. Morandat a également conseillé l'équipe sur le contenu du cahier des charges. Tout d'abord, celui-ci doit être rédigé, à la manière d'un rapport : les listes à puces doivent être utilisées avec parcimonie, uniquement dans certains cas spécifiques. Les diagrammes, en particulier les diagrammes standardisés (diagrammes de classes UML, diagrammes de séquence UML...) sont encouragés.
Par ailleurs, la granularité du cahier des charges doit être suffisamment fine, sans pour autant couvrir des détails trop spécifiques (boutons d'interface graphique, etc). En effet, tout élément spécifié dans le cahier des charges est contractuel : par conséquent, seuls les concepts doivent être spécifiés, pas les détails de l'interface utilisateur. Néanmoins, des maquettes d'exemple d'interface graphique (wireframes) peuvent être fournies.

## Travail à réaliser pour la réunion suivante

La prochaine réunion est programmée le mardi 5 janvier à 14h, et consistera à répartir les tâches entre les membres de l'équipe : M. Morandat jouera alors le rôle de chef de projet, non plus de client.

D'ici à la réunion suivante, l'équipe est invitée à commencer à rédiger le cahier des charges, notamment à proposer un plan détaillé ainsi que des chapôs.
Le système d'annotations peut également être spécifié davantage. En particulier, il est nécessaire de définir si une fenêtre peut posséder un ou plusieurs canevas, si des modèles de canevas peuvent être sauvegardés et réutilisés, etc. Ces différentes fonctionnalités peuvent être priorisées selon leur valeur métier.
