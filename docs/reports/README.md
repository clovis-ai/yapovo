# Comptes-rendus & Rapports

Ce répertoire contient les différents rapports et comptes-rendus de réunions rédigés.

La plupart d'entre eux sont rédigés en Markdown, en utilisant la syntaxe Pandoc. Cette syntaxe
Markdown ajoute des fonctionnalités qui ne sont **pas** supportées par la prévisualisation Markdown
de Gitlab, mais qui permettent de générer des fichiers PDF, HTML... plus élaborés.

Chaque fichier Markdown est accompagné d'un Makefile, qui génère un fichier PDF à partir du fichier
Markdown. Pour ce faire, il est indispensable d'installer :

- le compilateur LaTeX [XeTeX](http://xetex.sourceforge.net/), qui supporte l'UTF-8 et les fontes
  TTF et OTF
- l'outil de conversion [Pandoc](https://pandoc.org/), ainsi que le template pour
  Pandoc [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) :
  les instructions d'installation sont disponibles à l'adresse
  <https://github.com/Wandmalfarbe/pandoc-latex-template>

Une fois ces outils installés, il suffit de lancer la commande `make` depuis ce répertoire
(`docs/reports`) pour compiler tous les rapports en PDF.

Alternativement, chaque rapport peut être compilé individuellement. Par exemple, la commande :

```sh
make meeting-1/meeting-1.pdf
```

permet de compiler uniquement le compte-rendu de la réunion n°1.

Enfin, les fichiers PDF générés peuvent être supprimés avec la règle `clean` :

```sh
make clean
```
  
