---
lang: fr
subject: "Projet au Fil de l'Année"
title: "YAPOVO : Compte-rendu de réunion n°1"
date: "1er décembre 2020"
author:
  - "Ivan Canet"
  - "Antoine Chartron"
  - "Rémi Dehenne"
  - "Sébastien de Nerée"
  - "Sohaib Errabii"
  - "Maxime Girardet"
  - "Aurélien Moinel"
keywords: [PFA, Projet au Fil de l'Année, S7, ENSEIRB-MATMECA, YAPOVO, Overlay, Compte-Rendu 1]

output:
    pdf_document

urlcolor: RoyalBlue
listings-disable-line-numbers: true
footer-left : $~$

header-includes: |
  \newcommand{\thesubject}{Projet au Fil de l'Année}
  \newcommand{\thetutor}{Floréal Morandat}
  \renewcommand{\lstlistingname}{Code}
  \renewcommand{\lstlistlistingname}{Liste des \lstlistingname s}

numbersections: false

---

```{=latex}
  \begin{center}
    \Large\textbf{\thesubject{}\\\thetitle}\\[0.5\baselineskip]
    \large Client : \thetutor\\[0.5\baselineskip]
    \large Équipe : \theauthor\\[0.5\baselineskip]
    \large{\makeatletter\@date\makeatother}
  \end{center}
```

Mardi 1er décembre 2020, M. Morandat et l'équipe projet se sont réunis afin de définir l'objet et le périmètre du projet. À partir des pistes proposées par M. Morandat au cours de la réunion n°1, l'équipe projet a présenté deux «solutions» : une solution «sans overlay» et une «avec overlay». Le support de présentation est disponible à l'adresse <https://demo.hedgedoc.org/p/ACMMGVCbK#/>.

Finalement, les deux solutions ne doivent pas être opposées selon M. Morandat : la version «sans overlay» est une première milestone avant de livrer une version plus ergonomique, avec un overlay.

# Milestone 1 : Sans Overlay

D'une part, la version «sans overlay» nécessite d'alterner entre deux fenêtres : la fenêtre de l'application réelle et un onglet de navigateur.

L'utilisateur interagit normalement avec la fenêtre de l'application à annoter, tel un terminal ou un éditeur de code. Pour ajouter ou visualiser les annotations, il se rend dans l'onglet de navigateur correspondant. L'onglet de navigateur retransmet le flux vidéo de la fenêtre de l'application, sur lequel l'utilisateur peut dessiner des annotations.

Dans cette première milestone, l'utilisateur doit donc alterner entre la fenêtre de l'application et le navigateur, comme schématisé en \autoref{fig:sans-overlay}.

![Maquette simplifiée de la version «sans overlay» \label{fig:sans-overlay}](sans-overlay.png)

# Milestone 2 : Avec Overlay

La première version «sans overlay» n'est pas suffisante pour M. Morandat. En effet, l'alternance entre l'application à annoter et l'onglet du navigateur est peu ergonomique. Il serait préférable que les annotations apparaissent par-dessus l'application à annoter, sur un calque transparent : un overlay.
Une telle solution est représentée en \autoref{fig:avec-overlay}, dans laquelle l'overlay est schématisé en vert.

L'utilisateur peut alors rapidement alterner entre  l'application à annoter et le dessin d'annotations sur l'overlay, en utilisant par exemple des raccourcis clavier ou des touches de stylet de tablette graphique. Cependant, même si cette version est plus ergonomique, l'overlay doit être implémenté nativement pour chaque plateforme supportée, contrairement à la version «sans overlay».

![Maquette simplifiée de la version «avec overlay» \label{fig:avec-overlay}](avec-overlay.png){width=400px}


# Multiplexage de fenêtres et d'overlays

Par ailleurs, M. Morandat a mis en évidence un autre besoin : un utilisateur doit pouvoir annoter et partager plusieurs fenêtres, sans partager et annoter la totalité de l'écran.

Pour ce faire, l'utilisateur souhaite spécifier au démarrage la (ou les) fenêtre(s) à annoter : chacune de ces fenêtres dispose alors d'un overlay et d'annotations qui lui sont propres. 

Toutefois, même si plusieurs fenêtres peuvent être annotées, le logiciel développé doit générer un unique flux vidéo qui affiche la fenêtre choisie par l'utilisateur et ses annotations. Ce flux vidéo _en sortie_ doit pouvoir être partagé à d'autres applications, par exemple _Big Blue Button_, via le partage de fenêtre ou d'onglet du navigateur.

## Multiplexage

Enfin, le logiciel développé doit servir de _multiplexeur_, et permettre de modifier facilement la fenêtre et les annotations affichées dans le flux vidéo de sortie.

En particulier, dans la solution «sans overlay», la confirmation de partage d'une fenêtre par le navigateur ne doit apparaître qu'une seule fois, à l'ouverture de l'onglet (et non à chaque fois que l'utilisateur souhaite modifier la fenêtre à afficher dans le flux de sortie).

# Développement de POC (Proof Of Concept)

Finalement, afin d'éviter les «impasses» lors du développement, M. Morandat a insisté sur la nécessité de développer des POC (Proof Of Concept) avant de développer l'application attendue. Cette méthode permet d'identifier au plus tôt les potentiels éléments bloquants et de prévoir des solutions en amont.

En particulier, M. Morandat souhaite que l'équipe développe, pour une réunion ultérieure, une POC qui génère une seule image à partir de plusieurs images sources en Javascript. En effet, avant de multiplexer des flux vidéos, cette POC permettrait de se familiariser avec le multiplexage d'images, supposée plus simple. 

